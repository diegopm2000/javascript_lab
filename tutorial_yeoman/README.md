# Tutorial de Yeoman


El tutorial está basado en el tutorial (codelab) oficial de Yeoman

http://yeoman.io/codelab/index.html

La versión de Yeoman con la que se ha realizado es la 1.8.5


### 1: Instalación del entorno


Se necesita tener instalado node, npm y git

- Instalación de Node:

```shell
$ sudo apt-get install nodejs-legacy
$ node --version
```

- Instalación de npm:

```shell
$ sudo apt-get install npm
$ sudo npm install npm -g
$ npm -version
```

Para evitar problemas al acceder a los módulos de npm hay que seguir estos pasos:

https://docs.npmjs.com/getting-started/fixing-npm-permissions

```shell
$ npm config get prefix
$ sudo chown -R $(whoami) $(npm config get prefix)/lib/node_modules
```

- Instalación de git

```shell
$ sudo apt-get install git
$ git --version
```

Para el tutorial, partimos de las siguientes versiones ya instaladas:

- Node v4.6.2
- npm 3.10.9
- git 2.10.2

### 2: Instalación de un generador de Yeoman

Aquí se puede ver una lista de los más de 3500 generadores que hay actualmente en Yeoman.

http://yeoman.io/generators/

Vamos a instalar el generador siguiente:

https://www.npmjs.com/package/generator-fountain-webapp

```shell
Yeoman Fountain generator to scaffold a webapp with React or Angular written in ES6 (Babel),
TypeScript through Webpack or SystemJS including tools Gulp 4, ESLint, Browsersync and Karma
```

Para instalarlo, desde la raiz de nuestro proyecto vacío /Tutorial_Yeoman, tecleamos:

```shell
$ npm install --global generator-fountain-webapp
```

Esto nos lo instala de forma global.

También se puede instalar ejecutando:

```shell
$ yo
```

Y accediendo al menú de "Install a generator"


### 3: Usar el generador para hacer "scaffolding" de nuestra aplicación


Creamos la carpeta mytodo dentro de /Tutorial_Yeoman

```shell
$ mkdir mytodo && cd mytodo
```

Ahora accedemos a los generadores

```shell
$ yo
```

- Elegimos la opción "FountainWebapp"

NOTA: también se puede hacer lo mismo de forma más rápida:

```shell
$ yo fountain-webapp
```

Y a continuación se nos pide el framework de javascript  que queremos usar:

- Elegimos React

Se nos pide el gestor de módulos:

- Elegimos Webpack con npm

Se nos pide el preprocesador de JS:

- Elegimos ES2015 today with Babel

Se nos pide el preprocesador de CSS

- Elegimos SASS

Como plataforma de integración continua

- No marcamos ninguna

Como sampling app:

- Elegimos Redux Todomvc

Como router:

- No marcamos nada

Y nos empieza a crear la estructura del proyecto (tarda un poco).


### 4: Revisar todo lo que nos ha creado yeoman

```shell
/src --> Directorio de los fuentes de la aplicación

/src/app --> Codigo fuente de react

/src/index.html --> es la página principal de la aplicación

/src/index.js --> es el punto de entrada para la aplicacion TodoMVC que vamos a hacer

/conf --> fichero padre de configuración para herramientas de terceros (Browsersync, Webpack, Gulp, Karma)

/gulp_tasks y gulpfile.js --> Nuestras tareas de construcción

.babelrc, package.json y node_modules --> Configuración y dependencias requeridas

.gitattributes y .gitignore --> Configuración de git
```

Ejecutamos nuestro primer commit

```shell
$ git add --all && git commit -m 'First commit'
```

### 5: Visualizar nuestra aplicación en el Navegador

Para arrancar un servidor local basado en node, basta con hacer:

```shell
$ npm run serve
```

Nos arranca un servidor http://localhost:3000

Desde este enlace ya se puede acceder a la aplicación:

Para parar el servidor, basta con hacer __ctrol+c__

Vamos ahora, con el servidor arrancado a probar el livereload:

Editamos el fichero src/app/components/Header.js

Y modificamos en la línea 19 la palabra "todo" por "YO"

Editamos también el test de la ruta /src/app/components/Header.spec.js, modificando la palabra "todo" por "YO" en la línea 33.


### 6: Ejecutar los test con Kharma y Jasmine


Karma es un test-runner agnostico del framework. Fountainwebapp nos lo ha incluido como framework de test Jasmine.

Cuando ejecutamos el generador del proyecto de FountainWebapp, el generador escaneó todos los ficheros con el patrón

```shell
*.spec.js
```

creó un fichero conf/karma.conf.js y puso ahí los módulos de NodeJS para Karma. Más adelante, editaremos un fichero de Jasmine para describir nuestros test, pero primero vamos a ver como ejecutar los test que ya tenemos.

Paramos el servidor local de Node (ctrol+c)

Desde la raíz del proyecto, ejecutamos:

```shell
$ npm test
```

```shell
diego@alfacentauri:~/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo$ npm test

> @ test /home/diego/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo
> gulp test

[15:51:32] Loading /home/diego/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo/gulp_tasks/browsersync.js
[15:51:33] Loading /home/diego/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo/gulp_tasks/karma.js
[15:51:33] Loading /home/diego/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo/gulp_tasks/misc.js
[15:51:33] Loading /home/diego/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo/gulp_tasks/webpack.js
[15:51:34] Using gulpfile ~/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo/gulpfile.js
[15:51:34] Starting 'test'...
[15:51:34] Starting 'karma:single-run'...
21 12 2016 15:51:44.901:INFO [karma]: Karma v1.3.0 server started at http://localhost:9876/
21 12 2016 15:51:44.903:INFO [launcher]: Launching browser PhantomJS with unlimited concurrency
21 12 2016 15:51:44.910:INFO [launcher]: Starting browser PhantomJS
21 12 2016 15:51:45.889:INFO [PhantomJS 2.1.1 (Linux 0.0.0)]: Connected on socket /#Pv5007Io0n34IJE9AAAA with id 71629767
PhantomJS 2.1.1 (Linux 0.0.0): Executed 49 of 49 SUCCESS (0.177 secs / 0.128 secs)
[15:51:46] Finished 'karma:single-run' after 13 s
[15:51:46] Finished 'test' after 13 s
```

Como vemos, todos los test se han pasado correctamente.

Vamos ahora a actualizar algún test

Vamos al fichero

```shell
src/app/reducers/todos.spec.js
```

Observando el primer test:

```code
it('should handle initial state', () => {
  expect(todos(undefined, {})).toEqual([
    {
      text: 'Use Redux',
      completed: false,
      id: 0
    }
  ]);
});
```

Vamos a modificarlo por:

```code
it('should handle initial state', () => {
  expect(todos(undefined, {})).toEqual([
    {
      text: 'Use Yeoman', // <=== HERE
      completed: false,
      id: 0
    }
  ]);
});
```

(Sólo hemos cambiado el texto de 'Use Redux' por 'Use Yeoman'

Volvemos a ejecutar los test:

```shell
$ npm test
```

Y el test falla, como era de esperar:

```shell
$ npm test

> @ test /home/diego/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo
> gulp test

[15:56:28] Loading /home/diego/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo/gulp_tasks/browsersync.js
[15:56:29] Loading /home/diego/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo/gulp_tasks/karma.js
[15:56:30] Loading /home/diego/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo/gulp_tasks/misc.js
[15:56:30] Loading /home/diego/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo/gulp_tasks/webpack.js
[15:56:30] Using gulpfile ~/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo/gulpfile.js
[15:56:30] Starting 'test'...
[15:56:30] Starting 'karma:single-run'...
21 12 2016 15:56:40.803:INFO [karma]: Karma v1.3.0 server started at http://localhost:9876/
21 12 2016 15:56:40.805:INFO [launcher]: Launching browser PhantomJS with unlimited concurrency
21 12 2016 15:56:40.873:INFO [launcher]: Starting browser PhantomJS
21 12 2016 15:56:42.381:INFO [PhantomJS 2.1.1 (Linux 0.0.0)]: Connected on socket /#2Q2srAtGb9o0MWEhAAAA with id 5691055
PhantomJS 2.1.1 (Linux 0.0.0) todos reducer should handle initial state FAILED
	Expected [ Object({ text: 'Use Redux', completed: false, id: 0 }) ] to equal [ Object({ text: 'Use Yeoman', completed: false, id: 0 }) ].
	src/index.spec.js:22081:56
PhantomJS 2.1.1 (Linux 0.0.0): Executed 49 of 49 (1 FAILED) (0.125 secs / 0.079 secs)
[15:56:42] 'karma:single-run' errored after 13 s
[15:56:42] Error: Failed 1 tests.
    at /home/diego/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo/gulp_tasks/karma.js:13:22
    at removeAllListeners (/home/diego/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo/node_modules/karma/lib/server.js:379:7)
    at Server.<anonymous> (/home/diego/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo/node_modules/karma/lib/server.js:390:9)
    at Server.g (events.js:260:16)
    at emitNone (events.js:72:20)
    at Server.emit (events.js:166:7)
    at emitCloseNT (net.js:1535:8)
    at nextTickCallbackWith1Arg (node.js:447:9)
    at process._tickDomainCallback (node.js:410:17)
[15:56:42] 'test' errored after 13 s
npm ERR! Test failed.  See above for more details.

```

NOTA: Se puede hacer que los test se ejecuten de forma automática al cambiar algo usando: npm run test:auto

Abrimos el fichero: src/app/reducers/todos.js y lo modificamos (cambiamos 'Use Redux' por 'Use Yeoman'):

```code
const initialState = [
  {
    text: 'Use Yeoman',
    completed: false,
    id: 0
  }
];
```

Volvemos a pasar los test:

```shell
$ npm test


> @ test /home/diego/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo
> gulp test

[16:00:33] Loading /home/diego/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo/gulp_tasks/browsersync.js
[16:00:34] Loading /home/diego/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo/gulp_tasks/karma.js
[16:00:35] Loading /home/diego/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo/gulp_tasks/misc.js
[16:00:35] Loading /home/diego/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo/gulp_tasks/webpack.js
[16:00:35] Using gulpfile ~/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo/gulpfile.js
[16:00:35] Starting 'test'...
[16:00:35] Starting 'karma:single-run'...
21 12 2016 16:00:46.585:INFO [karma]: Karma v1.3.0 server started at http://localhost:9876/
21 12 2016 16:00:46.588:INFO [launcher]: Launching browser PhantomJS with unlimited concurrency
21 12 2016 16:00:46.599:INFO [launcher]: Starting browser PhantomJS
21 12 2016 16:00:47.647:INFO [PhantomJS 2.1.1 (Linux 0.0.0)]: Connected on socket /#t13UkwyfmCPvGO5CAAAA with id 68080407
PhantomJS 2.1.1 (Linux 0.0.0): Executed 49 of 49 SUCCESS (0.201 secs / 0.127 secs)
[16:00:48] Finished 'karma:single-run' after 13 s
[16:00:48] Finished 'test' after 13 s

```

### 7: Implementar persistencia con almacenamiento local

Para ello vamos a usar el módulo de Redux llamado redux-localstorage

Lo instalamos:

```shell
$ npm install --save redux-localstorage@rc


/home/diego/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo
├── redux-localstorage@1.0.0-rc5
└── UNMET PEER DEPENDENCY webpack@2.1.0-beta.27

npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@^1.0.0 (node_modules/chokidar/node_modules/fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.0.15: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})
npm WARN webpack-fail-plugin@1.0.5 requires a peer of webpack@^1.9.11 but none was installed.
npm WARN mytodo No description
npm WARN mytodo No repository field.
npm WARN mytodo No license field.

```

Para configurar el redux-localstorage

Nos vamos al fichero: src/app/store/configureStore.js

Y reeemplazamos todo lo que tenga por esto:

```code

import {compose, createStore} from 'redux';
import rootReducer from '../reducers';

import persistState, {mergePersistedState} from 'redux-localstorage';
import adapter from 'redux-localstorage/lib/adapters/localStorage';

export default function configureStore(initialState) {
  const reducer = compose(
    mergePersistedState()
  )(rootReducer, initialState);

  const storage = adapter(window.localStorage);

  const createPersistentStore = compose(
    persistState(storage, 'state')
  )(createStore);

  const store = createPersistentStore(reducer);
  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers').default;
      store.replaceReducer(nextReducer);
    });
  }

  return store;
}

```

Si nos fijamos ahora, en el servidor de node, vemos que nos muestra un elemento, de nombre 'Use Yeoman' en la lista de todos.

Añadimos desde el navegador unos cuantos elementos

Y desde el Navegador Chrome, accedemos a la pestaña de persistencia local, y ahí podemos ver que aparecen las entradas que hemos ido poniendo.


### 8: Preparar la aplicación para ser instalada en Producción


Para preparar nuestra aplicación para el entorno de Producción, vamos a querer hacer lo siguiente:

- Pasar lint a nuestro código
- Concatenar y minimificar nuestro código y estilos para ahorro de ancho de banda en el funcionamiento
- Compilar la salida usando los precompiladores que estemos usando

Todo esto se puede hacer con lo siguiente:

```shell
$ npm run build


diego@alfacentauri:~/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo$ npm run build

> @ build /home/diego/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo
> gulp

[17:03:15] Loading /home/diego/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo/gulp_tasks/browsersync.js
[17:03:16] Loading /home/diego/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo/gulp_tasks/karma.js
[17:03:17] Loading /home/diego/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo/gulp_tasks/misc.js
[17:03:17] Loading /home/diego/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo/gulp_tasks/webpack.js
[17:03:17] Using gulpfile ~/workspace/Laboratorios/Javascript/Tutorial_Yeoman/mytodo/gulpfile.js
[17:03:17] Starting 'default'...
[17:03:17] Starting 'clean'...
[17:03:17] Finished 'clean' after 53 ms
[17:03:17] Starting 'build'...
[17:03:17] Starting 'other'...
[17:03:17] Starting 'webpack:dist'...
[17:03:17] Finished 'other' after 253 ms
[17:03:34] Time: 17073ms
                                     Asset       Size  Chunks             Chunk Names
               app-5d1797e803db6b328567.js     102 kB       0  [emitted]  app
            vendor-5d1797e803db6b328567.js     217 kB       1  [emitted]  vendor
index-9bec0618e8eb95eddab5c0c39540617c.css     5.2 kB       0  [emitted]  app
                                index.html  806 bytes          [emitted]  
Child html-webpack-plugin for "index.html":
         Asset    Size  Chunks       Chunk Names
    index.html  544 kB       0       
Child extract-text-webpack-plugin:

[17:03:34] Finished 'webpack:dist' after 17 s
[17:03:34] Finished 'build' after 17 s
[17:03:34] Finished 'default' after 17 s

```

Esto nos ha empaquetado todo el código en la carpeta /dist

Si queremos ahora ejecutar lo que hemos construido como entragable, hacemos:

```shell
$ npm run serve:dist
```

### Para finalizar

Podemos ver que generadores tenemos disponibles ya instalados en nuestro Yeoman así:

```shell
$ yo --generators
```

__Enlaces de interés:__

yeoman.io --> Página principal de Yeoman
http://fountainjs.io/ --> Página de Fountain.js
https://facebook.github.io/react/ --> Página del framework React de Facebook
https://angular.io/ --> Página del framework Angular
https://webpack.github.io/ --> Página de webpack. A module bundler who takes modules with dependencies and generates static assets representing those modules.
http://jspm.io/ --> A frictionless browser package management. Load any module format (ES6, AMD, CommonJS and globals) directly from any registry such as npm and GitHub with flat versioned dependency management.
