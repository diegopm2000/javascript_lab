# Laboratorio de Módulos en javascript

El laboratorio se basa en el artículo de esta web:

https://medium.freecodecamp.com/javascript-modules-a-beginner-s-guide-783f7d7a5fcc#.r3eloyfjk

Entender el sistema de gestión de módulos de javascript es vital para los desarrolladores.

## 1. Ventajas de usar módulos

- __Mantenibilidad__: Mejora el mantenimiento de la aplicación al tener cada funcionalidad encapsulada en un módulo y no tener todo repartido por doquier. También facilita las mejoras y el crecimiento de la aplicación.

- __Namespacing__: En javascript, las funciones en el ámbito más alto son globales. Usando el namespacing, se evita llenar el ámbito global con morralla. El compartir variables entre partes del código usando el ámbito global es una muy mala práctica. Los módulos nos permiten crear un ámbito privado de compartición de variables.

- __Reutilización__: Nos permite reutilizar un módulo en otra aplicación en lugar de tener que hacer copia-pega de una parte del código de otro proyecto.

## 2. ¿Como incorporar los módulos?


### 2.1 Patrón módulo

El patrón módulo es una forma de "mimetizar" el comportamiento de clases en Java habitual hacia javasript, ya que nativamente carece de estas.

De esta forma, podemos almacenar variables en un objeto, así como encapsular métodos públicos y privados dentro de ella.

Hay varias formas de lograr esto:

#### 2.1.1 Usando una cláusula (closure)

Construimos una función anómina y dentro hemos definido variables como propiedades y variables como funciones.

```javascript
(function () {
  // We keep these variables private inside this closure scope

  var myGrades = [93, 95, 88, 0, 55, 91];

  var average = function() {
    var total = myGrades.reduce(function(accumulator, item) {
      return accumulator + item}, 0);

      return 'Your average grade is ' + total / myGrades.length + '.';
  }

  var failing = function(){
    var failingGrades = myGrades.filter(function(item) {
      return item < 70;});

    return 'You failed ' + failingGrades.length + ' times.';
  }

  console.log(failing());

}());

// ‘You failed 2 times.’
```

Esta aproximación es un buen inicio, pero no aisla del ámbito global y nos permite seguir usando una variable definida en un ámbito global, por ejemplo:

```javascript
var global = 'Hello, I am a global variable :)';

(function () {
  // We keep these variables private inside this closure scope

  var myGrades = [93, 95, 88, 0, 55, 91];

  var average = function() {
    var total = myGrades.reduce(function(accumulator, item) {
      return accumulator + item}, 0);

    return 'Your average grade is ' + total / myGrades.length + '.';
  }

  var failing = function(){
    var failingGrades = myGrades.filter(function(item) {
      return item < 70;});

    return 'You failed ' + failingGrades.length + ' times.';
  }

  console.log(failing());
  console.log(global);
}());

// 'You failed 2 times.'
// 'Hello, I am a global variable :)'
```

__NOTA__: Los paréntesis que recubren la función son necesarios, ya que javascript considera siempre cualquier sentencia que empiece por function como una declaración de función (no se puede tener declaración de funciones anónimas en javascript). De está forma, conseguimos que lo interprete como una expresión de función en lugar de una declaración.

#### 2.1.2 Usando un import global

Otra técnica común, usada por librerías como jquery, es usar un import global. Es similar a la anterior, salvo que ahora pasamos las variables globales como parámetros.

```javascript
(function (globalVariable) {

  // Keep this variables private inside this closure scope
  var privateFunction = function() {
    console.log('Shhhh, this is private!');
  }

  // Expose the below methods via the globalVariable interface while
  // hiding the implementation of the method within the
  // function() block

  globalVariable.each = function(collection, iterator) {
    if (Array.isArray(collection)) {
      for (var i = 0; i < collection.length; i++) {
        iterator(collection[i], i, collection);
      }
    } else {
      for (var key in collection) {
        iterator(collection[key], key, collection);
      }
    }
  };

  globalVariable.filter = function(collection, test) {
    var filtered = [];
    globalVariable.each(collection, function(item) {
      if (test(item)) {
        filtered.push(item);
      }
    });
    return filtered;
  };

  globalVariable.map = function(collection, iterator) {
    var mapped = [];
    globalUtils.each(collection, function(value, key, collection) {
      mapped.push(iterator(value));
    });
    return mapped;
  };

  globalVariable.reduce = function(collection, iterator, accumulator) {
    var startingValueMissing = accumulator === undefined;

    globalVariable.each(collection, function(item) {
      if(startingValueMissing) {
        accumulator = item;
        startingValueMissing = false;
      } else {
        accumulator = iterator(accumulator, item);
      }
    });

    return accumulator;

  };

 }(globalVariable));

```

En este ejemplo, la variable "globalVariable" es la única variable global. El beneficio de esta aproximación respecto a las cláusulas es que declaramos una variable global arriba del todo facilitando la comprensión de lectura del código.

#### 2.1.3 Usando un Interfaz objeto

Otra aproximación es crear módulos usando un objeto autocontenido a modo de interfaz.

```javascript
var myGradesCalculate = (function () {

  // Keep this variable private inside this closure scope
  var myGrades = [93, 95, 88, 0, 55, 91];

  // Expose these functions via an interface while hiding
  // the implementation of the module within the function() block

  return {
    average: function() {
      var total = myGrades.reduce(function(accumulator, item) {
        return accumulator + item;
        }, 0);

      return'Your average grade is ' + total / myGrades.length + '.';
    },

    failing: function() {
      var failingGrades = myGrades.filter(function(item) {
          return item < 70;
        });

      return 'You failed ' + failingGrades.length + ' times.';
    }
  }
})();

myGradesCalculate.failing(); // 'You failed 2 times.'
myGradesCalculate.average(); // 'Your average grade is 70.33333333333333.'
```

Con esto ya podemos decidir que variables y métodos son públicos o privadas. En nuestro caso, dejamos myGrades como privada, y en el return ponemos los métodos que queremos exponer como públicos (average y failing).

#### 2.1.4 Patrón del módulo revelador

Es similar al caso anterior, pero aquí nos asegura que todo es privado salvo que lo expongamos

```javascript
var myGradesCalculate = (function () {

  // Keep this variable private inside this closure scope
  var myGrades = [93, 95, 88, 0, 55, 91];

  var average = function() {
    var total = myGrades.reduce(function(accumulator, item) {
      return accumulator + item;
      }, 0);

    return'Your average grade is ' + total / myGrades.length + '.';
  };

  var failing = function() {
    var failingGrades = myGrades.filter(function(item) {
        return item < 70;
      });

    return 'You failed ' + failingGrades.length + ' times.';
  };

  // Explicitly reveal public pointers to the private functions
  // that we want to reveal publicly

  return {
    average: average,
    failing: failing
  }
})();

myGradesCalculate.failing(); // 'You failed 2 times.'
myGradesCalculate.average(); // 'Your average grade is 70.33333333333333.'
```

## 3. Common JS y AMD

Todas las aproximaciones anteriores tienen algo en común: se usa una variable global para encapsular dentro una función que contiene la lógica del módulo, creando así un espacio de nombres privado para sí mismo utilizando una cláusula.

Aunque es bastante efectiva, tiene una serie de problemas:

- Como desarrollador, necesitas conocer el orden correcto de las dependencias para poder usar estos módulos. Por ejemplo, si usas __backbone__, necesitarás cargar el fichero de backbone en tú código fuente para poder utilizarlo. Cómo __backbone__ tiene una dependencia clara con Underscore, hay que poner delante de la carga del fichero de Backbone el de Underscore. Esto puede complicar mucho la gestión de las dependencias si tenemos muchos ficheros a importar con interdependencia entre ellos.

- Otro problema es la colisión que puede haber en el espacio de nombres, al ir todo al ámbito global, si dos módulos tienen el mismo nombre, o bien, si tenemos varias versiones del mismo módulo.

La pregunta que viene ahora es si se puede hacer esto sin contaminar el ámbito global de la aplicación.

La respuesta es sí: hay dos implementaciones __Common JS__ y __AMD__


### 3.1 Common JS

Common JS es un grupo de voluntarios que diseña e implementa API's para poder declarar módulos.

Un módulo de Common JS es básicamente una pieza de código reutilizable en javascript, que exporta unos objetos específicos dejándolos disponibles para otros módulos o partes del programa que los necesiten. Si conoces Node JS, el formato te será muy familiar.

Con Common JS, cada módulo tiene su ámbito local (como si fuera encapsulado en una cláusula). Usamos module.exports para exponer el módulo y require para utilizarlo.

```javascript
function myModule() {
  this.hello = function() {
    return 'hello!';
  }

  this.goodbye = function() {
    return 'goodbye!';
  }
}

module.exports = myModule;
```


Y ahora para usar el módulo:

```javascript
var myModule = require('myModule');

var myModuleInstance = new myModule();
myModuleInstance.hello(); // 'hello!'
myModuleInstance.goodbye(); // 'goodbye!'
```

Con esto, ya hemos conseguido las siguientes mejoras:

- Evitamos contaminar el ámbito global de la aplicación.
- Exponemos explícitamente nuestras dependencias
- La sintaxis es muy compacta y muchísimo más clara que con las closures.

Hay que tener en cuenta que la aproximación de Common JS es "server side", y carga los módulos de forma síncrona. Esto para una aplicación frontal puede no resultar práctico. Además, hay que tener en cuenta que en el frontal suele tardar más la carga de módulos que en el servidor, ya que los tiene mucho más cerca. Además, durante la carga de módulos, bloquea al browser impidiéndole ejecutar otra cosa en javascript (recordar que es monohilo) hasta que se haya cargado todo completamente.

### 3.2 AMD

Para solucionar este problema, aparece AMD, que posibilita la carga de módulos de forma asíncrona.

La carga de módulos usando AMD tiene esta forma:

```javascript
define(['myModule', 'myOtherModule'], function(myModule, myOtherModule) {
  console.log(myModule.hello());
});
```

Lo que hacemos es definir una función que toma como argumentos los módulos que queremos cargar y son cargados en background, de una forma no bloqueante, usando la asincronía de javascript, llamando a la función de callback definida cuando haya terminado.

La función de callback toma como argumentos las dos dependencias definidas en este caso (myModule y myOtherModule), permitiendo a la función usar estas dependencias. En definitiva, las dependencias son definidas usando la palabra __define__.

Por ejemplo, nuestro módulo __myModule__ podría tener esta forma:

```javascript
define([], function() {

  return {
    hello: function() {
      console.log('hello');
    },
    goodbye: function() {
      console.log('goodbye');
    }
  };
});
```

AMD está orientado a trabajar en "browser side".

Otra ventaja aparte de esta asincronicidad, es que AMD soporta que los módulos sean objetos, constructores, cadenas, JSON y muchos otros tipos. Mientras que CommonJS sólo soporta objetos como módulos.

Como desventaja, AMD no soporta io, sistema de ficheros u otras características orientadas a servidor.

### 3.3 UMD

Para proyectos en los que sea necesario soportar las características tanto de Common JS como de AMD, aparece UMD (Universal Module Definition).

De esta forma, con UMD podemos trabajar tanto en la parte servidor como en la del browser sin problema.

```javascript
(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
      // AMD
    define(['myModule', 'myOtherModule'], factory);
  } else if (typeof exports === 'object') {
      // CommonJS
    module.exports = factory(require('myModule'), require('myOtherModule'));
  } else {
    // Browser globals (Note: root is window)
    root.returnExports = factory(root.myModule, root.myOtherModule);
  }
}(this, function (myModule, myOtherModule) {
  // Methods
  function notHelloOrGoodbye(){}; // A private method
  function hello(){}; // A public method because it's returned (see below)
  function goodbye(){}; // A public method because it's returned (see below)

  // Exposed public methods
  return {
      hello: hello,
      goodbye: goodbye
  }
}));
```

### Native JS

Todo lo anteriormente visto no es realmente nativo a javascript, sino que son técnicas o artificios para conseguir algo que se asemeje a lo que es un módulo.

Afortunadamente, el grupo TC39 (los que definen el ECMAScript) han decidido introducir en ECMAScript 6 un sistema de módulos nativo por fin.

ES6 ofrecerá varias posibilidades de importación/exportación de módulos.

Lo mejor de ES6 es que ofrecerá lo mejor de ambos mundos (Common JS y AMD), es decir la compacidad y claridad, y la asíncronia, además de un soporte a dependencias cíclicas.

Probablemente, una de las novedades interesantes serán los imports, que serán vistas read-only de los exports en lugar de copias del código de los exports como en Common JS, y consecuentemente no son "live".

Por ejemplo:

```javascript
// lib/counter.js

var counter = 1;

function increment() {
  counter++;
}

function decrement() {
  counter--;
}

module.exports = {
  counter: counter,
  increment: increment,
  decrement: decrement
};


// src/main.js

var counter = require('../../lib/counter');

counter.increment();
console.log(counter.counter); // 1
```
En este ejemplo, mantenemos dos copias del módulo, una cuando la exportamos y otra cuando la importamos. Lo importante es que la copia del módulo en main.js está desconectada del módulo en sí. Eso nos impide modificar el contador en main.js, ya que al ejecutar el incremento, no afecta realmente al valor (la vista del módulo es read only)

La única forma de modificarlo, es hacerlo manualmente.

```javascript
counter.counter++;
console.log(counter.counter); // 2
```

Es decir, que lo que tenemos es una copia read-only del módulo.

```javascript
// lib/counter.js
export let counter = 1;

export function increment() {
  counter++;
}

export function decrement() {
  counter--;
}


// src/main.js
import * as counter from '../../counter';

console.log(counter.counter); // 1
counter.increment();
console.log(counter.counter); // 2
```
