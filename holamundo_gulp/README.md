# Hola Mundo Gulp

Se sigue la guía de: https://www.adictosaltrabajo.com/tutoriales/primeros-pasos-con-gulp/

Para el holamundo Gulp hemos hecho:

### 1. Instalamos el cliente de Gulp

```shell
$ sudo npm install --global gulp-cli
```

### 2. Creamos la estructura de un proyecto (package.json)

```shell
$ npm init
```

### 3. Instalamos Gulp en el proyecto con la opción dev

```shell
$ npm install gulp --save-dev
```

Esto nos va a dejar el fichero package.json con gulp como devDependencies:

```code
{
  "name": "holamundogulp",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "",
  "license": "ISC",
  "devDependencies": {
    "gulp": "^3.9.1"
  }
}
```

### 4. Creamos un fichero gulpfile.js con lo siguiente:

```code
var gulp = require('gulp');

gulp.task('default', function() {
    console.log('Hello world!');
});
```


### 5. Ejecutar Gulp

```shell
$ gulp
```

Obtenemos en la salida lo siguiente:

```shell
[12:55:23] Using gulpfile ~/workspace/Laboratorios/Javascript/HolaMundo_Gulp/gulpfile.js
[12:55:23] Starting 'default'...
Hello world Gulp!
[12:55:23] Finished 'default' after 160 μs
```

### 6. Ahora creamos la carpeta src/js

```shell
$ mkdir src/js
```

### 7. Dentro de esta carpeta, creamos los siguientes ficheros:

file1.js

Con el contenido:

```code
function add(paramA, paramB) {
  console.log('This is a sum');
  return paramA + paramB;
}
```

file2.js

Con el contenido:

```code
function substract(arg0, arg1) {
  console.log('This is a substract');
  return arg0 - arg1;
}
```

### 8. Vamos a crear una nueva tarea de nombre minify.

Esta tarea lo que hace es concatenar todo el código y ofuscarlo. Para ello, se usan los plugins concat y uglify. Los instalamos:

```shell
$ npm install --save-dev gulp-concat
```

```shell
$ npm install --save-dev gulp-uglify
```

### 9. Añadimos en el fichero gulpfile.js los "require" necesarios y la nueva task minify

```code
var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

gulp.task('minify', function() {
  console.log('minifying js ...');

  return gulp.src('src/js/*.js')
    .pipe(concat('all.js'))
    .pipe(uglify({
      compress: {
        drop_console: true
      }
    }))
    .pipe(gulp.dest('build/js/'));
});

gulp.task('default', function() {
    console.log('Hello world Gulp!');
});
```

### 10. Ejecutamos la tarea que hemos creado

```shell
$ gulp minify
```

Esto lo que hace es

- Coge las fuentes de src/js
- Concatena todo lo que haya hacia un fichero all.js
- Con el plugin uglify con la opción compress, elimina comentarios console.log, espacios sobrantes, y ofusca el código.
- Finalmente el resultado lo deja en el directorio build/js
