module.exports = function(grunt) {  // Toda la configuración se encontará dentro de está funcion

  grunt.initConfig({    // Iniciamos la configuración de Grunt

    pkg: grunt.file.readJSON('package.json'),   // Leemos el archivo packageJSON, lo que nos permitirá usar la información contenida en el archivo a lo largo de esta función

    concat: {   // Iniciamos la configuración del plugin concat

      options: {

        separator: ';'  // Este es el caracter que separará unos ficheros de otros cuando estos se junten en un solo archivo.

      },
      dist: {

        src: ['src/**/*.js'],   // Este es un patrón para seleccionar los archivos que deseamos concatenar
        dest: 'js/<%= pkg.name %>.js'   // Este es el archivo de salida y la carpeta donde se va a encontrar, en este caso usa la variable de nombre de paquete que hay en package.json

      }
    },
    uglify: {

      options: {

        banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'    // Un texto que se presentará al principio del archivo

      },
      dist: {

        files: {

          'js/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']  // en este caso, coge el directorio de salida del task de concatenado y lo minifica cambiándole el nombre

        }
      }
    },
    jasmine : {
      src : 'src/**/*.js',          // El directorio donde se encuentran el código que tendrá que pasar los test
      options : {
        specs : 'spec/**/*.js'      // El directorio donde se encuentran los tests
      }
    },
    jshint: {
      all: [                        // Todos los archivos que pasaran a través del jsHint
        'Gruntfile.js',             // Pasaremos también el propio  Gruntfile
        'src/**/*.js',
        'spec/**/*.js'              // Y los archivos de test.
      ],
      options: {
        jshintrc: '.jshintrc'       // Indicamos donde se encuentra el archivo de opciones de jsHint.
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');   // Aquí tenemos que añadir los módulos de npm que vayamos a usar
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-jasmine');

  grunt.registerTask('default', ['concat','uglify']); // Estas son las distintas task que queremos usar, la ruta por defecto se invoca escribiendo grunt a secas.
  grunt.registerTask('onlyConcat', ['concat']); // Este task se ejecutaría escribiendo grunt onlyConcat, para desarrollo nos interesaría no minificar los archivos.
  grunt.registerTask('test', ['jshint', 'jasmine']);
  grunt.registerTask('build', ['jshint','jasmine','concat','uglify']);

};
