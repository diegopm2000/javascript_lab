# HolaMundo de Grunt

### 1. Instalamos el cliente de Grunt de forma global

```shell
$ npm install -g grunt-cli
```

### 2. Creamos la estructura inicial del proyecto

```shell
$ npm init
```

### 3. Instalación del cliente de Grunt como devDependencies

```shell
$ npm install grunt --save-dev
```

### 4. Vamos a usar dos plugins: concat y uglify, los instalamos

```shell
$ npm install grunt-contrib-concat --save-dev

$ npm install grunt-contrib-uglify --save-dev
```

### 5. Creamos el fichero gruntfile.js

Ver fichero en el disco, no lo pongo aquí porque es muy grande

### 6. Ejecutamos la tarea por defecto de grunt

```shell
$ grunt
```

Nos crea en la carpeta js dos ficheros, con el nombre del package del proyecto (holamundogrunt)

- holamundogrunt.js -> todos los ficheros .js concatenados en uno solo
- holamundogrunt.min.js -> todos los ficheros .js concatenados en uno solo y minimificados.

### 7. Vamos a usar jasmine para correr los test unitarios y jshint para verificar la calidad del código.

Añadimos al script gruntfile.js lo siguiente:

```code
jasmine : {
  src : 'src/**/*.js',          // El directorio donde se encuentran el código que tendrá que pasar los test
  options : {
    specs : 'spec/**/*.js'      // El directorio donde se encuentran los tests
  }
},
jshint: {
  all: [                        // Todos los archivos que pasaran a través del jsHint
    'Gruntfile.js',             // Pasaremos también el propio  Gruntfile
    'src/**/*.js',
    'spec/**/*.js'              // Y los archivos de test.
  ],
  options: {
    jshintrc: '.jshintrc'       // Indicamos donde se encuentra el archivo de opciones de jsHint.
  }
}
```

Y también añadimos al final:

```code
grunt.registerTask('test', ['jshint', 'jasmine']);

grunt.registerTask('build', ['jshint','jasmine','concat','uglify']);
```

### 8. Instalamos los paquetes como devDependencies

```shell
$ npm install grunt-contrib-jshint --save-dev
$ npm install grunt-contrib-jasmine --save-dev
```

### 9. Creamos un fichero de configuración .jshintrc (sin el no funciona)

```shell
{
    "bitwise": true,
    "curly": true,
    "eqeqeq": true,
    "forin": true,
    "freeze": true,
    "futurehostile": true,
    "latedef": true,
    "maxcomplexity": 4,
    "maxdepth": 2,
    "maxparams": 2,
    "maxstatements": 5,
    "noarg": true,
    "nocomma": true,
    "nonew": true,
    "undef": true,
    "unused": true,
    "console":false
}
```


### 10. Ejecutamos la tarea de verificación de calidad del código

```shell
$ grunt test
```
