//index.js

var HolaMundoDIFactory = require("./app/HolaMundoDI_Factory");

//Creates the factory
var myFactory = new HolaMundoDIFactory();

//CASE 1. ENG

//Uses the factory to obtain implementation ENG
var miHelloWorld = myFactory.build("ENG");

//Print log using the language implementation
console.log("ESP: "+miHelloWorld.getMessage());

//CASE 2. ESP

//Uses the factory to obtain implementation ESP
var miHolaMundo = myFactory.build("SPA");

//Print log using the language implementation
console.log("ENG: "+miHolaMundo.getMessage());

//CASE 3. DEFAULT

//Uses the factory to obtain implementation Default
var miHolaMundoDefault = myFactory.build("OTRA COSA");

//Print log using the language implementation
console.log("DEFAULT: "+miHolaMundoDefault.getMessage());
