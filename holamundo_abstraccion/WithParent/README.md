# Implementación de inyección de dependencias usando parent como declaración de contrato en javascript

### 1. Implementaciones específicas

Tenemos dos implementaciones distintas:

```javascript
//HolaMundoDI_ENG.js

function HolaMundoDI_ENG() {
  //Default constructor
}

HolaMundoDI_ENG.prototype.getMessage = function() {
  return "Hello World Dependency Injection!";
}

module.exports = HolaMundoDI_ENG;
```


```javascript
//HolaMundoDI_ESP.js

function HolaMundoDI_ESP() {
  //Default constructor
}

HolaMundoDI_ESP.prototype.getMessage = function() {
  return "¡Hola Mundo Inyección de Dependencias!";
}

module.exports = HolaMundoDI_ESP;
```

Cada una de ellas lo que hace es implementar una función __getMessage()__ que devuelve un mensaje en un idioma distito.

### 2. Parent a modo de capa de abtracción

Añadimos un Parent, que proporciona abstracción sobre la implementación, ya que encapsula la implementación en una variable y define el contrato que han de cumplir cada una de las implementaciones

```javascript
//HolaMundoDI.js

function HolaMundoDI() {
  this.messageBuilder = null;
}

HolaMundoDI.prototype.setMessageBuilder = function(messageBuilder) {
  this.messageBuilder = messageBuilder;
}

HolaMundoDI.prototype.getMessage = function() {
  return this.messageBuilder.getMessage();
}

module.exports = HolaMundoDI;
```

### 3. Factoría

Desacoplamos tanto de las implementaciones específicas como del Parent la obtención de una instancia del parent usando una factoría e inyección de dependencias

```javascript
//HolaMundoDI_Factory.js

var HolaMundoDI = require ("../app/HolaMundoDI");

function HolaMundoDI_Factory() {
  //Default constructor
}

HolaMundoDI_Factory.prototype.build = function(language) {

  //Select the lib required for the specification language
  var languageMessageBuilderFile;
  if (language === 'ES') {
    languageMessageBuilderFile = "../app/HolaMundoDI_ESP";
  } else if (language === 'ENG') {
    languageMessageBuilderFile = "../app/HolaMundoDI_ENG";
  } else {
    languageMessageBuilderFile = "../app/HolaMundoDI_ESP";
  }
  //Set the specification library
  var MessageBuilder = require (languageMessageBuilderFile);

  //Instantiates the Parent
  var holaMundoDI = new HolaMundoDI();
  //Sets the implementation of the messagebuilder
  holaMundoDI.setMessageBuilder(new MessageBuilder());
  //Return the parent with implementation into
  return holaMundoDI;
}

module.exports = HolaMundoDI_Factory;
```

### 4. Fichero de ejemplo de uso

Hemos definido un index.js que usará todos estos patrones que hemos visto.

```javascript
//index.js

var HolaMundoDIFactory = require("./app/HolaMundoDI_Factory");

//Creates the factory
var myFactory = new HolaMundoDIFactory();

//CASE 1. ENG

//Uses the factory to obtain implementation ENG
var miHelloWorld = myFactory.build("ENG");

//Print log using the language implementation
console.log("ESP: "+miHelloWorld.getMessage());

//CASE 2. ESP

//Uses the factory to obtain implementation ESP
var miHolaMundo = myFactory.build("SPA");

//Print log using the language implementation
console.log("ENG: "+miHolaMundo.getMessage());

//CASE 3. DEFAULT

//Uses the factory to obtain implementation Default
var miHolaMundoDefault = myFactory.build("OTRA COSA");

//Print log using the language implementation
console.log("DEFAULT: "+miHolaMundoDefault.getMessage());
```

### 5. Utilización

Instalación:

```shell
$ npm install
```

Ejecutamos, desde la carpeta donde se encuentra el index.js:

```shell
$ node index.js

ESP:Hello World Dependency Injection!
ENG:¡Hola Mundo Inyección de Dependencias!
DEFAULT¡Hola Mundo Inyección de Dependencias!
```

### 6. Test unitarios

Hemos utilizado chai y mocha para los test unitarios.

Para lanzar los test unitarios:

```shell
$ npm test
```
