//HolaMundoDI_Factory.js

var HolaMundoDI = require ("../app/HolaMundoDI");

function HolaMundoDI_Factory() {
  //Default constructor
}

HolaMundoDI_Factory.prototype.build = function(language) {

  //Select the lib required for the specification language
  var languageMessageBuilderFile;
  if (language === 'ES') {
    languageMessageBuilderFile = "../app/HolaMundoDI_ESP";
  } else if (language === 'ENG') {
    languageMessageBuilderFile = "../app/HolaMundoDI_ENG";
  } else {
    languageMessageBuilderFile = "../app/HolaMundoDI_ESP";
  }
  //Set the specification library
  var MessageBuilder = require (languageMessageBuilderFile);

  //Instantiates the Parent
  var holaMundoDI = new HolaMundoDI();
  //Sets the implementation of the messagebuilder
  holaMundoDI.setMessageBuilder(new MessageBuilder());
  //Return the parent with implementation into
  return holaMundoDI;
}

module.exports = HolaMundoDI_Factory;
