//HolaMundoDI.js

function HolaMundoDI() {
  this.messageBuilder = null;
}

HolaMundoDI.prototype.setMessageBuilder = function(messageBuilder) {
  this.messageBuilder = messageBuilder;
}

HolaMundoDI.prototype.getMessage = function() {
  return this.messageBuilder.getMessage();
}

module.exports = HolaMundoDI;
