var expect    = require("chai").expect;
var HolaMundoDIFactory = require("../app/HolaMundoDI_Factory");

describe("Abtraccion con Parent - Test", function() {

  var myFactory;

  beforeEach(function() {

    myFactory = new HolaMundoDIFactory();
  });

  describe("Construcción del builder con mensaje en español", function() {
    it("Crea la clase que nos devuelve el mensaje en español", function() {

      var miMessageBuilder = myFactory.build("SPA");

      expect(miMessageBuilder.getMessage()).to.equal("¡Hola Mundo Inyección de Dependencias!");
    });

  });

  describe("Construcción del builder con mensaje en ingles", function() {
    it("Crea la clase que nos devuelve el mensaje en ingles", function() {

      var miMessageBuilder = myFactory.build("ENG");

      expect(miMessageBuilder.getMessage()).to.equal("Hello World Dependency Injection!");
    });

  });

  describe("Construcción del builder con mensaje por defecto", function() {
    it("Crea la clase que nos devuelve el mensaje por defecto", function() {

      var miMessageBuilder = myFactory.build("Otra cosa");

      expect(miMessageBuilder.getMessage()).to.equal("¡Hola Mundo Inyección de Dependencias!");
    });

  });

});
