//HolaMundoDI_Factory.js

function HolaMundoDI_Factory() {
  //Default constructor
}

HolaMundoDI_Factory.prototype.build = function(language) {

  //Select the lib required for the specification language
  var languageMessageBuilderFile;
  if (language === 'ES') {
    languageMessageBuilderFile = "../app/HolaMundoDI_ESP";
  } else if (language === 'ENG') {
    languageMessageBuilderFile = "../app/HolaMundoDI_ENG";
  } else {
    languageMessageBuilderFile = "../app/HolaMundoDI_ESP";
  }
  //Set the specification library
  var MessageImpl = require (languageMessageBuilderFile);

  //Return the parent with implementation into
  return new MessageImpl();
}

module.exports = HolaMundoDI_Factory;
