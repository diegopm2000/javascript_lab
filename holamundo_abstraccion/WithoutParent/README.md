# Implementación de abtracción usando factoría (sin contrato en un parent) en javascript

En este caso, nos valemos simplemente de una factoría y duck typing para hacer la abastracción.

### 1. Implementaciones específicas

Tenemos dos implementaciones distintas:

```javascript
//HolaMundoDI_ENG.js

function HolaMundoDI_ENG() {
  //Default constructor
}

HolaMundoDI_ENG.prototype.getMessage = function() {
  return "Hello World Dependency Injection!";
}

module.exports = HolaMundoDI_ENG;
```


```javascript
//HolaMundoDI_ESP.js

function HolaMundoDI_ESP() {
  //Default constructor
}

HolaMundoDI_ESP.prototype.getMessage = function() {
  return "¡Hola Mundo Inyección de Dependencias!";
}

module.exports = HolaMundoDI_ESP;
```

Cada una de ellas lo que hace es implementar una función __getMessage()__ que devuelve un mensaje en un idioma distito.

### 2. Factoría

En este caso, no usamos un parent que encapsule la implementación. Por tanto no hacemos inyección de dependencias inyectando la implementación específica en el builder, sino que simplemente usando el duck typing de javascript devolvemos una implementación u otra. De esta forma, la factoría misma, "inyecta" la implementación que necesitamos devolver.

Como desventaja, perdemos la definición del contrato que han de implementar cada una de las implementaciones específicas, ganando libertad y flexibilidad, ya que cada una de las implementaciones podría implementar diferentes métodos.

```javascript
//HolaMundoDI_Factory.js

function HolaMundoDI_Factory() {
  //Default constructor
}

HolaMundoDI_Factory.prototype.build = function(language) {

  //Select the lib required for the specification language
  var languageMessageBuilderFile;
  if (language === 'ES') {
    languageMessageBuilderFile = "../app/HolaMundoDI_ESP";
  } else if (language === 'ENG') {
    languageMessageBuilderFile = "../app/HolaMundoDI_ENG";
  } else {
    languageMessageBuilderFile = "../app/HolaMundoDI_ESP";
  }
  //Set the specification library
  var MessageImpl = require (languageMessageBuilderFile);

  //Return the parent with implementation into
  return new MessageImpl();
}

module.exports = HolaMundoDI_Factory;
```

### 4. Fichero de ejemplo de uso

Hemos definido un index.js que usará todos estos patrones que hemos visto.

```javascript
//index.js

var HolaMundoDIFactory = require("./app/HolaMundoDI_Factory");

//Creates the factory
var myFactory = new HolaMundoDIFactory();

//CASE 1. ENG

//Uses the factory to obtain implementation ENG
var miHelloWorld = myFactory.build("ENG");

//Print log using the language implementation
console.log("ESP: "+miHelloWorld.getMessage());

//CASE 2. ESP

//Uses the factory to obtain implementation ESP
var miHolaMundo = myFactory.build("SPA");

//Print log using the language implementation
console.log("ENG: "+miHolaMundo.getMessage());

//CASE 3. DEFAULT

//Uses the factory to obtain implementation Default
var miHolaMundoDefault = myFactory.build("OTRA COSA");

//Print log using the language implementation
console.log("DEFAULT: "+miHolaMundoDefault.getMessage());
```

### 5. Utilización

Instalación:

```shell
$ npm install
```

Ejecutamos, desde la carpeta donde se encuentra el index.js:

```shell
$ node index.js

ESP:Hello World Dependency Injection!
ENG:¡Hola Mundo Inyección de Dependencias!
DEFAULT¡Hola Mundo Inyección de Dependencias!
```

### 6. Test unitarios

Hemos utilizado chai y mocha para los test unitarios.

Para lanzar los test unitarios:

```shell
$ npm test
```
