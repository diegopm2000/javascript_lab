# Hola Mundo Abstracción en Javascript

Se han implementado dos soluciones:

### Solución WithParent

Esta solución usa un parent que define el contrato que deberían implementar los módulos específicos.

Usa una factoría para decidir que implementación "inyectar" al parent.

Patrones que usamos:

- Factoría
- Inyección de dependencias
- Usamos como interfaz de exportación de nuestros módulos un Constructor

### Solución WithoutParent

Esta solución se basa simplemente en usar el duck typing que proporciona el lenguaje Javascript y que permite más flexibilidad al no tener necesariamente que cumplir las implementaciones específicas un contrato.

- Factoría
- Inyección de dependencias
- Usamos como interfaz de exportación de nuestros módulos un Constructor

NOTA: ver este enlace para conocer las 7 formas que hay al menos de exportar algo en un módulo de Node JS:

https://team.goodeggs.com/export-this-interface-design-patterns-for-node-js-modules-b48a3b1f8f40#.vmrc1sz2n
