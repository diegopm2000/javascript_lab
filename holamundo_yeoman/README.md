# HolaMundo Yeoman

### 1. Instalación de yeoman de forma global

```shell
$ npm install -g yo
```

Nos instala la version 1.8.5

### 2. Instalación de un generador de webapp

```shell
$ npm install -g generator-webapp
```

### 3. Scaffolding básico

```shell
$ yo webapp
```

Elegimos:

- Bootstrap
- BDD

Nos crea toda la estructura del proyecto de una aplicacion web tipica

```code
/app
/bower_components
/node_modules
/test
bower.json
gulpfile.js
package.json
```

### 4. Para probar la aplicación

Levantamos el servidor con gulp

```shell
$ gulp serve
```
