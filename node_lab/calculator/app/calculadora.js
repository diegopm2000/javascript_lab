//calculadora.js

function Calculadora() {
  //Constructor
}

Calculadora.prototype.add = function(a, b) {
  return a + b;
}

Calculadora.prototype.minus = function(a, b) {
  return a - b;
}

Calculadora.prototype.multiply = function(a, b) {
  return a * b;
}

Calculadora.prototype.divide = function(a, b) {
  return a / b;
}

module.exports = Calculadora;
