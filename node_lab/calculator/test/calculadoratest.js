var expect    = require("chai").expect;
var Calculadora = require("../app/calculadora");

describe("Calculadora - Test", function() {

  var miCalculadora;

  beforeEach(function() {

    miCalculadora = new Calculadora();
  });

  describe("Testing the Operations", function() {

    it("Testing the sum operation", function() {

      expect(miCalculadora.add(1,1)).to.equal(1+1);
    });

    it("Testing the minus operation", function() {

      expect(miCalculadora.minus(1,1)).to.equal(1-1);
    });

    it("Testing the multiply operation", function() {

      expect(miCalculadora.multiply(3,3)).to.equal(3*3);
    });


    it("Testing the divide operation", function() {

      expect(miCalculadora.divide(3,3)).to.equal(3/3);
    });

  });
});
