# Calculadora en Node JS

Implementación de una calculadora de números enteros con las siguientes operaciones:

* Add
* Minus
* Multiply
* Divide

Para la __creación inicial__ del fichero descriptor package.json hemos ejecutado:

```shell
npm init
```
En el directorio __app__ se encuentra el código fuente de la calculadora.

En el directorio __test__ se encuentran las pruebas unitarias.

## Verificación de calidad del código con Mocka, Chai, Istambul y Sonar

### Pruebas unitarias

Se implementan las pruebas unitarias usando las siguientes librerías:

* __mocka__  para los test unitarios
* __chai__ para los matchers

Para lanzar las pruebas hacemos:

```shell
$ npm test
```

### Cobertura de las pruebas

Usamos la librería __istanbul__ para verificar el porcentaje de la cobertura de las pruebas.

Hemos configurado en el package.json el test de tal forma que adeḿas de llamarse a mocka se llame a istanbul.

### SonarQube

Previamente, hemos instalado y añadido en el path la herramienta __sonar-scanner__

Hemos configurado el fichero sonar-project.properties adecuadamente

```code
sonar.dynamicAnalysis=reuseReports

sonar.javascript.jstest.reportsPath=coverage
sonar.javascript.lcov.reportPath=coverage/lcov.info
```

Lanzamos sonar con:

```shell
$ sonar-scanner
```
