'use strict';

const DataTypes = require('sequelize');

module.exports = function(sequelize) {
  const user = sequelize.define('User', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING
    },
    surname: {
      type: DataTypes.STRING
    },
    loginname: {
      type: DataTypes.STRING
    },
    enabled: {
      type: DataTypes.BOOLEAN
    }
  });

  return user;
}
