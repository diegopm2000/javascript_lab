'use strict';

const DataTypes = require('sequelize');

module.exports = function(sequelize) {
  const role = sequelize.define('Role', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING
    }
  });

  return role;
}
