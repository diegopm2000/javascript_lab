# Laboratorio de modelado con Sequelize (Parte 1)

Se han introducido modificaciones respecto al laboratorio de Sequelize - HolaMundo.

Ahora, los dos modelos (Role y User) se encuentran en ficheros separados.

Para importarlos, se ha usado el import específico de sequelizes
