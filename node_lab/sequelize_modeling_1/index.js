'use strict';

var Sequelize = require('sequelize');

const sequelize = new Sequelize('mysql://root:mysecretpassword@localhost:3306/dbtest');

var Role = sequelize.import('./model/role');
var User = sequelize.import('./model/user');

////////////////////////////////////////////////////////////////////////////////
// Initial Data
////////////////////////////////////////////////////////////////////////////////

var roleAdmin = { id: '1', name: 'Admin' };
var roleConsumer = { id: '2', name: 'Consumer' };
var roleCommerceAdmin = { id: '3', name: 'CommerceAdmin' };
var roleEmployeeAdmin = { id: '4', name: 'EmployeeAdmin' };

var userDperez = {id: '1', name: 'Diego', surname: 'Perez Molinero', loginname: 'dperez', enabled: true, role_id: 1};

////////////////////////////////////////////////////////////////////////////////
// Operations
////////////////////////////////////////////////////////////////////////////////

sequelize.authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  //Make the associations
  .then(() => {
    User.belongsTo(Role, { as: 'role', foreignKey: 'role_id' });
    Role.hasMany(User, { foreignKey: 'role_id', as: 'users' });
  })
  .then(() => {
    return sequelize.sync({force: true})
  })
  .then(() => {
    return Role.create(roleAdmin);
  })
  .then(() => {
    return Role.create(roleConsumer);
  })
  .then(() => {
    return Role.create(roleCommerceAdmin);
  })
  .then(() => {
    return Role.create(roleEmployeeAdmin);
  })
  .then(() => {
    return User.create(userDperez);
  })
  .then(() => {
    return sequelize.models.Role.findAll({include: [ { model: sequelize.models.User, as: 'users' } ]})
  })
  .then(roles => {
    console.log("Roles: --->");
    for (var i=0; i<roles.length; i++) {
      console.log(roles[i].dataValues)
    }
    return true;
  })
  .then(() => {
    return User.findAll({include: [ { model: Role, as: 'role' } ]})
  })
  .then(users => {
    console.log("Users: --->");
    for (var i=0; i<users.length; i++) {
      console.log(users[i].dataValues)
    }
  })
  .then(() => {
    sequelize.close();
    console.log("Connection closed!");
  })
  .catch(err => {
    console.error('Error:', err);
  });
