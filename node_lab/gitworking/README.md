# Working with Git - Alternatives

## Download a file using directly a URL:

How to download the __last version__ of a file using a URL.

Could be tested using Postman or Browser.

### Bitbucket

How to:

https://bitbucket.org/'owner'/'repository'/raw/'branch'/'filename'

Example:

https://bitbucket.org/diegopm2000/javascript_lab/raw/master/README.md


NOTE: Case of feature

https://bitbucket.org/'owner'/'repository'/raw/'feature'/'branch'/'filename'


### GitHub

How to:

https://raw.githubusercontent.com/'user'/'repository'/'branch'/'filename'

Example:

https://raw.githubusercontent.com/sameersbn/docker-gitlab/master/docker-compose.yml

### Gitlab

How to:

We need the token in Gitlab. Go to Settings Account ang get the token.

Then, use this kind of url:

http://gitlab.com/<group_name>/<project_name>/raw/master/<f‌​older>/<file_name>?p‌​rivate_token=<your_k‌​ey>

Example:

Our token is: EUdhXh1h8zHsNzAk-dDV

If the project is public, the token is unnecesary.

http://localhost:10080/root/Prueba/raw/master/README.md?private_token=EUdhXh1h8zHsNzAk-dDV

http://localhost:10080/root/Prueba/raw/master/README.md

## Download a file using curl

### Bitbucket

How to:

```shell
$ curl -L -O https://bitbucket.org/'owner'/'repository'/raw/'branch'/'filename'
```


Example:

```shell
$ curl -L -O https://bitbucket.org/diegopm2000/javascript_lab/raw/master/README.md

```

### GitHub

How to:

```shell
$ curl -L -O https://raw.githubusercontent.com/'user'/'repository'/'branch'/'filename'
```

Example:

```shell
$ curl -L -O https://raw.githubusercontent.com/sameersbn/docker-gitlab/master/docker-compose.yml
```

### GitLab

How to:

```shell
$ curl -L -O http://gitlab.com/<group_name>/<project_name>/raw/master/<f‌​older>/<file_name>?p‌​rivate_token=<your_k‌​ey>
```

Example:

Our token is: EUdhXh1h8zHsNzAk-dDV

```shell
$ curl -L http://localhost:10080/root/Prueba/raw/master/README.md?private_token=EUdhXh1h8zHsNzAk-dDV
```

Without token:

```shell
$ curl -L -O http://localhost:10080/root/Prueba/raw/master/README.md
```


### Download a file using wget

#### Bitbucket

How to:

```shell
$ wget https://bitbucket.org/'owner'/'repository'/raw/'branch'/'filename'
```

Example:

```shell
$ wget https://bitbucket.org/diegopm2000/javascript_lab/raw/master/README.md
```

#### GitHub

How to:

```shell
$ wget https://raw.githubusercontent.com/'user'/'repository'/'branch'/'filename'
```

Example:

```shell
$ wget https://raw.githubusercontent.com/sameersbn/docker-gitlab/master/docker-compose.yml
```

#### Gitlab

How to:

```shell
$ wget -O <file_name> http://gitlab.com/<group_name>/<project_name>/raw/master/<f‌​older>/<file_name>?p‌​rivate_token=<your_k‌​ey>
```

Example:

```shell
$ wget -O README.md http://localhost:10080/root/Prueba/raw/master/README.md?private_token=EUdhXh1h8zHsNzAk-dDV
```

Whitout token:

```shell
$ wget http://localhost:10080/root/Prueba/raw/master/README.md
```

### Using the library node-wget

```javascript

var wget = require('node-wget');

wget({
        url:  'https://bitbucket.org/diegopm2000/javascript_lab/raw/master/README.md',
        dest: './tmpbitbucket/',      // destination path or path with filenname, default is ./
        timeout: 2000       // duration to wait for request fulfillment in milliseconds, default is 2 seconds
    },
    function (error, response, body) {
        if (error) {
            console.log('--- error:');
            console.log(error);            // error encountered
        } else {
            console.log('--- headers:');
            console.log(response.headers); // response headers
            console.log('--- body:');
            console.log(body);             // content of package
        }
    }
);
```
