# Laboratorio de control de asincronía en Javascript

Laboratorio extraído de aquí:

https://www.todojs.com/controlar-la-ejecucion-asincrona/

Javascript en un lenguaje __mono-hilo__ y __síncrono__.

## 1. Función que tomaremos como base para el laboratorio

Primero definimos una función __asyncSqrt__ que calcula el cuadrado de un número. Hemos introducido un timeout para dotar a la función de una ejecución __asíncrona__.

Fichero async.js

```Javascript
"use strict";

function asyncSqrt(value, callback) {
    console.log('START execution with value =', value);
    setTimeout(function() {
        callback(value, value * value);
    }, 0 | Math.random() * 100);
}

asyncSqrt(2, function(value, result) {
    console.log('END execution with value =', value, 'and result =', result);
});
console.log('COMPLETED ?');
```

La función __Math.random__ devuelve un número aleatorio entre 0 y 1, por lo que tenemos un timeout que va a estar entre 0 y 100 mili segundos. Además usamos el operador | (or binario) para convertir a entero de 32 bits rápidamente el resultado:

```javascript
0 | Math.random() * 100
```

En la llamada a __asyncSqrt__ pasamos como segundo parámetro una función de callback, que es lo que queremos que se ejecute cuando vuelva.

La función de callback se puede expresar también con la __función flecha__ (arrow function):

```Javascript
asyncSqrt(2, (value, result) => {
    console.log('END execution with value =', value, 'and result =', result);
});
```

Si ejecutamos, tendremos la siguiente salida:

```shell
$ node async.js
START execution with value = 2
COMPLETED ?
END execution with value = 2 and result = 4
```

Veamos el orden de ejecución:

1. Se ejecuta la __definición__ de la función asyncSqrt
2. Se ejecuta asyncSqrt, da un salto a la función e imprime:

  ```shell
  START execution with value = 2
  ```

3. Se define el setTimeout
4. Se ejecuta la impresión de:

  ```shell
  COMPLETED ?
  ```

5. Finalmente, vence el timeout despues de n mili segundos, y se imprime:

  ```shell
  END execution with value = 2 and result = 4
  ```

Vamos ahora a añadir un bucle for y lanzar la función async 10 veces

Fichero asyncBucle.js

```Javascript
"use strict";

function asyncSqrt(value, callback) {
    console.log('START execution with value =', value);
    setTimeout(function() {
        callback(value, value * value);
    }, 0 | Math.random() * 100);
}

for (var n = 0; n < 10; n++) {
    asyncSqrt(n, function(value, result) {
        console.log('END execution with value =', value, 'and result =', result);
    });
}
console.log('COMPLETED ?');
```

Si ejecutamos ahora esto, obtenemos lo siguiente:

```shell
$ node asyncBucle.js
START execution with value = 0
START execution with value = 1
START execution with value = 2
START execution with value = 3
START execution with value = 4
START execution with value = 5
START execution with value = 6
START execution with value = 7
START execution with value = 8
START execution with value = 9
COMPLETED ?
END execution with value = 1 and result = 1
END execution with value = 4 and result = 16
END execution with value = 7 and result = 49
END execution with value = 5 and result = 25
END execution with value = 0 and result = 0
END execution with value = 3 and result = 9
END execution with value = 8 and result = 64
END execution with value = 6 and result = 36
END execution with value = 9 and result = 81
END execution with value = 2 and result = 4
```

Se han devuelto los resultados en función de como han ido acabando en el timeout (recordar que cada timeout era aleatorio)

Normalmente las funciones asíncronas no avisan que son asíncronas salvo que cuentan con un callback, aunque puede haber funciones síncronas que también tengan un callback.

Nuestro primer trabajo va a ser controlar que el COMPLETED aparezca al final de todas las ejecuciones.

En este caso, como conocemos el número de ejecuciones, basta con poner un contador para controlarlo:

Fichero asyncBucleYContador.js

```Javascript
"use strict";

function asyncSqrt(value, callback) {
    console.log('START execution with value =', value);
    setTimeout(function() {
        callback(value, value * value);
    }, 0 | Math.random() * 100);
}

var max = 10;
var cnt = 0;
for (var l = 0; l < max; l++) {
    asyncSqrt(l, function(value, result) {
        console.log('END execution with value =', value, 'and result =', result);
        if (++cnt === max) {
            console.log('COMPLETED');
        }
    });
}
```

Aprovechamos que javascript nos permite declarar una variable __cnt__, la que usamos como contador, fuera de la función callback y esta función aunque no se lo hemos pasado como parámetro, está manteniendo dentro de su ámbito de ejecución el conocimiento de esa variable. Otros lenguajes no lo permitirían.

Ejecutando esto, ahora obtenemos la salida con el COMPLETED al final:

```shell
$ node asyncBucleYContador.js
START execution with value = 0
START execution with value = 1
START execution with value = 2
START execution with value = 3
START execution with value = 4
START execution with value = 5
START execution with value = 6
START execution with value = 7
START execution with value = 8
START execution with value = 9
END execution with value = 0 and result = 0
END execution with value = 9 and result = 81
END execution with value = 4 and result = 16
END execution with value = 1 and result = 1
END execution with value = 5 and result = 25
END execution with value = 6 and result = 36
END execution with value = 3 and result = 9
END execution with value = 8 and result = 64
END execution with value = 2 and result = 4
END execution with value = 7 and result = 49
COMPLETED
```

Todavía nos faltaría arreglar el problema de que los resultados no se muestran en orden. Realmente se están mostrando en el orden en que han terminado, no en el orden en que han empezado a ajecutarse.

Una primera aproximación, y que nos sirve de ejemplo para ver lo que es la pirámide del infierno de los callback sería esta:

Fichero asyncBucleCallbackHell.js

```javascript
"use strict";

function asyncSqrt(value, callback) {
    console.log('START execution with value =', value);
    setTimeout(function () {
        callback(value, value * value);
    }, 0 | Math.random() * 100);
}

asyncSqrt(0, function (value, result) {
    console.log('END execution with value =', value, 'and result =', result);
    asyncSqrt(1, function (value, result) {
        console.log('END execution with value =', value, 'and result =', result);
        asyncSqrt(2, function (value, result) {
            console.log('END execution with value =', value, 'and result =', result);
            asyncSqrt(3, function (value, result) {
                console.log('END execution with value =', value, 'and result =', result);
                asyncSqrt(4, function (value, result) {
                    console.log('END execution with value =', value, 'and result =', result);
                    asyncSqrt(5, function (value, result) {
                        console.log('END execution with value =', value, 'and result =', result);
                        asyncSqrt(6, function (value, result) {
                            console.log('END execution with value =', value, 'and result =', result);
                            asyncSqrt(7, function (value, result) {
                                console.log('END execution with value =', value, 'and result =', result);
                                asyncSqrt(8, function (value, result) {
                                    console.log('END execution with value =', value, 'and result =', result);
                                    asyncSqrt(9, function (value, result) {
                                        console.log('END execution with value =', value, 'and result =', result);
                                        console.log('COMPLETED');
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
});
```

Funcionar funciona bien y se obtiene esta salida:

```shell
$ node asyncBucleCallbackHell.js
START execution with value = 0
END execution with value = 0 and result = 0
START execution with value = 1
END execution with value = 1 and result = 1
START execution with value = 2
END execution with value = 2 and result = 4
START execution with value = 3
END execution with value = 3 and result = 9
START execution with value = 4
END execution with value = 4 and result = 16
START execution with value = 5
END execution with value = 5 and result = 25
START execution with value = 6
END execution with value = 6 and result = 36
START execution with value = 7
END execution with value = 7 and result = 49
START execution with value = 8
END execution with value = 8 and result = 64
START execution with value = 9
END execution with value = 9 and result = 81
COMPLETED
```

Pero con el "callback hell" no hemos obtenido un código nada elegante ni muy legible.


Una alternativa fácil sería encadenar las llamadas aprovechando el contador que teenemos definido fuera y sin usar el bucle for. Además ya no usamos la función callback de forma anónima sino que le damos un nombre __callback__ para poder usarla desde el else. Usamos en este caso la recursividad.

fichero asyncEncadenados.js

```javascript
"use strict";

function asyncSqrt(value, callback) {
    console.log('START execution with value =', value);
    setTimeout(function() {
        callback(value, value * value);
    }, 0 | Math.random() * 100);
}

var max = 10;
var cnt = 0;
asyncSqrt(cnt, function callback(value, result) {
    console.log('END execution with value =', value, 'and result =', result);
    if (++cnt === max) {
        console.log('COMPLETED');
    } else {
        asyncSqrt(cnt, callback);
    }
});
```

Existe una colección de librerías como async con metodos __async-foreach__, __async.parallel()__ y async.waterfall__ para controlar todo esto.

También se puede usar una función propia, que en este caso se llama __forEachAll__

```javascript
"use strict";

function forEachAll(data, each, finish, sync) {
    var n = -1, result = [];
    var next = sync ?
        function () {
            if (++n < data.length) { each(data[n], result, next); }
            else if (finish)       { finish(result); }
        } :
        (function () {
            function completed() {
                if (++n <= data.length && finish) { finish(result); }
            }
            for (var i = 0; i < data.length; i++) { each(data[i], result, completed); }
            return completed;
        }());
    next();
}

function asyncSqrt(value, callback) {
    console.log('START execution with value =', value);
    setTimeout(function() {
        callback(value, value * value);
    }, 0 | Math.random() * 100);
}

forEachAll([0,1,2,3,4,5,6,7,8,9],
    function(value, allresult, next) {
        asyncSqrt(value, function(value, result) {
            console.log('END execution with value =', value, 'and result =', result);
            allresult.push({value: value, result: result});
            next();
        });

    },
    function(allresult) {
        console.log('COMPLETED');
        console.log(allresult);
    },
    true
);

```

El uso de forEachAll() es bastante sencillo:

- El primer parámetro es una matriz con todos los valores que queremos procesar.
- El segundo parámetro es la función que realiza la ejecución para cada llamada. Esta función recibe el valor que le corresponde, una matriz para guardar los resultados y una función next() para llamar cuando se haya completado el paso.
- El tercer parámetro es la función que se ejecutará al finalizar la ejecución completa y recibe en una matriz el resultado de todas las llamadas.
- Por último, el cuatro parámetro indica si la llamada se hace de forma secuencial (true) o de forma paralela (false). Prueba a cambiar este último parámetro y vuelve a ejecutar el ejemplo.

Verás que el resultado es completamente diferente.
Durante bastante tiempo hemos utilizado hemos utilizado forEachAll() en sistemas en producción con resultados muy buenos, por lo que es una buena alternativa para gestionar la asincronía en los casos donde los datos de entrada pueden ser matrices de valores.

Ejecutando, da este resultado:


```shell
START execution with value = 0
END execution with value = 0 and result = 0
START execution with value = 1
END execution with value = 1 and result = 1
START execution with value = 2
END execution with value = 2 and result = 4
START execution with value = 3
END execution with value = 3 and result = 9
START execution with value = 4
END execution with value = 4 and result = 16
START execution with value = 5
END execution with value = 5 and result = 25
START execution with value = 6
END execution with value = 6 and result = 36
START execution with value = 7
END execution with value = 7 and result = 49
START execution with value = 8
END execution with value = 8 and result = 64
START execution with value = 9
END execution with value = 9 and result = 81
COMPLETED
[ { value: 0, result: 0 },
  { value: 1, result: 1 },
  { value: 2, result: 4 },
  { value: 3, result: 9 },
  { value: 4, result: 16 },
  { value: 5, result: 25 },
  { value: 6, result: 36 },
  { value: 7, result: 49 },
  { value: 8, result: 64 },
  { value: 9, result: 81 } ]
```

## Promesas

Hay otros mecanismos, como las promesas, que formaban ya parte de otros lenguajes. Se han añadido a javascript recientemente.

Fichero asyncPromesas.js

```javascript
"use strict";

function promiseSqrt(value){
    console.log('START execution with value =', value);
    return new Promise(function (fulfill, reject){
        setTimeout(function() {
            fulfill({ value: value, result: value * value });
        }, 0 | Math.random() * 100);
    });
}

promiseSqrt(2).then(function(obj) {
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
});
console.log('COMPLETED ?');
```

Y nos da como ejecución:

```shell
$ node asyncPromesas.js
START execution with value = 2
COMPLETED ?
END execution with value = 2 and result = 4
```

Para el caso en que llamabamos a la función 10 veces:

```javascript
"use strict";

function promiseSqrt(value){
    return new Promise(function (fulfill, reject){
        console.log('START execution with value =', value);
        setTimeout(function() {
            fulfill({ value: value, result: value * value });
        }, 0 | Math.random() * 100);
    });
}

var p = [];
for (var n = 0; n < 10; n++) {
    p.push(promiseSqrt(n, n * 2));
}
Promise.all(p).then(function(results) {
    results.forEach(function(obj) {
        console.log('END execution with value =', obj.value, 'and result =', obj.result);
    });
    console.log('COMPLETED');
});
```

El control de que se hayan completado todas, lo podemos hacer insertando las llamadas a la función dentro de un array y luego llamando a Promise.all(p), siendo p el array de llamadas.

```shell
$ node asyncPromesasAll.js
START execution with value = 0
START execution with value = 1
START execution with value = 2
START execution with value = 3
START execution with value = 4
START execution with value = 5
START execution with value = 6
START execution with value = 7
START execution with value = 8
START execution with value = 9
END execution with value = 0 and result = 0
END execution with value = 1 and result = 1
END execution with value = 2 and result = 4
END execution with value = 3 and result = 9
END execution with value = 4 and result = 16
END execution with value = 5 and result = 25
END execution with value = 6 and result = 36
END execution with value = 7 and result = 49
END execution with value = 8 and result = 64
END execution with value = 9 and result = 81
COMPLETED
```

Aunque promise.All haya ejecutado los datos de forma desordenada, devuelve los datos ordenados en función del orden de entrada.

Si queremos ejecutarlos en cadena, tendremos el "promise hell"

```javascript
"use strict";

function promiseSqrt(value){
    return new Promise(function (fulfill, reject){
        console.log('START execution with value =', value);
        setTimeout(function() {
            fulfill({ value: value, result: value * value });
        }, 0 | Math.random() * 100);
    });
}

promiseSqrt(0).then(function(obj) {
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    return promiseSqrt(1);
}).then(function(obj) {
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    return promiseSqrt(2);
}).then(function(obj) {
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    return promiseSqrt(3);
}).then(function(obj) {
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    return promiseSqrt(4);
}).then(function(obj) {
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    return promiseSqrt(5);
}).then(function(obj) {
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    return promiseSqrt(6);
}).then(function(obj) {
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    return promiseSqrt(7);
}).then(function(obj) {
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    return promiseSqrt(8);
}).then(function(obj) {
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    return promiseSqrt(9);
}).then(function(obj) {
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
}).catch(function(err) {
    console.error(err);
});
```

Se obtiene el mismo resultado que antes, pero ya no queda tan elegante el código.

```shell
$ node asyncPromesasHell.js
START execution with value = 0
END execution with value = 0 and result = 0
START execution with value = 1
END execution with value = 1 and result = 1
START execution with value = 2
END execution with value = 2 and result = 4
START execution with value = 3
END execution with value = 3 and result = 9
START execution with value = 4
END execution with value = 4 and result = 16
START execution with value = 5
END execution with value = 5 and result = 25
START execution with value = 6
END execution with value = 6 and result = 36
START execution with value = 7
END execution with value = 7 and result = 49
START execution with value = 8
END execution with value = 8 and result = 64
START execution with value = 9
END execution with value = 9 and result = 81
```
Un aspecto importante de las promesas es que cualquier retorno dentro de un then de una promesa, se convierte a promesa.

## Uso de __reduce__ con promesas

```javascript
"use strict";

function promiseSqrt(value){
    return new Promise(function (fulfill, reject){
        console.log('START execution with value =', value);
        setTimeout(function() {
            fulfill({ value: value, result: value * value });
        }, 0 | Math.random() * 100);
    });
}

var p = [0,1,2,3,4,5,6,7,8,9];
p.reduce(
    function (sequence, value) {
        return sequence.then(function() {
            return promiseSqrt(value);
        }).then(function(obj) {
            console.log('END execution with value =', obj.value,
                        'and result =', obj.result);
        });
    },
    Promise.resolve()
).then(function() {
    console.log('COMPLETED');
});
```

Vamos a explicar paso a paso lo que hacemos en este caso:

1) En primer lugar se ha creado una matriz con todos los valores que queremos procesar y hemos llamado al método reduce() que tiene dos parámetros:

  - el primero es una función que será llamada para cada uno de los elementos de la matriz y que recibe cuatro parámetros, aunque en este caso sólo vamos a utilizar los dos primeros, que corresponden al valor previo y al valor actual de la matriz.
  - el segundo es opcional y corresponde al valor que se pasará a la función al iniciar el recorrido de la matriz.

2) Como valor inicial de la función reduce() estamos pasando Promise.resolve() que es una promesa que damos ya por correcta. Esto es así para que la primera llamada a secuence.then() se ejecute correctamente. En la siguientes llamadas sequence corresponde a la promesa anterior que es devuelta por promiseSqrt().

3) Cada una de las llamadas a promiseSqrt() devuelve una promesa, que como hemos dicho, es pasada como valor anterior a la función del reduce() y por lo tanto no se ejecuta hasta que no se ha terminado la ejecución anterior.

4) Para finalizar hay otra instrucción .then() que se ejecuta cuando se han concluido todas las ejecuciones anteriores.
Puede parecer un poco enrevesado, pero una vez que se conoce sólo hay que copiar este modelo y aplicarlo en cada una de las situaciones que nos podemos encontrar en los que queremos ejecutar una misma promesa sobre diferentes valores.

## Funciones generadoras

Otra aproximación es utilizar funciones generadoras. Este tipo de funciones son una potente funcionalidad de ES6 y están disponibles de forma nativa en Node.js 4.x, Chrome 39, Firefox 26.0, Edge 13, Safari 10 y Opera 26. Para hacer una llamada secuencial con generadores deberemos llamar a la función next() dentro del callback, tal y como nos han indicado Javier Miguel y Javier Abadía. El código utiliza asyncSqrt() que definimos más arriba y es este:

```javascript
"use strict";

function asyncSqrt(value, callback) {
    console.log('START execution with value =', value);
    setTimeout(function() {
        callback(value, value * value);
    }, 0 | Math.random() * 100);
}

function* gen(callback) {
    for (var i = 0; i < 10; i++) {
        yield asyncSqrt(i, callback);
    }
}
var iterator = gen(function (value, result) {
    console.log('END execution with value =', value, 'and result =', result);
    iterator.next();
});
iterator.next();
```

Como se puede ver el resultado es bastante manejable. Básicamente se utiliza una función generadora, se instancia y se hacen llamadas a next(), una fuera de la función para iniciar el proceso y el resto están dentro de la función que es pasada como callback.

La función generadora devuelve un iterador. Cuando se llama a iterator.next(), se ejecuta dentro de la función gen hasta el yield. Cada vez que se hace next, se ejecuta hasta el siguiente yield. Así hasta que salga del bucle for.

Es importante tener en cuenta que las funciones generadoras no se pueden pasar como callbacks.

__Un error común: usar yield dentro de un callback definido dentro de la función generadora__

Algo que resulta poco intuitivo es que yield solo se puede incluir dentro de una función generadora. En ocasiones podemos insertar en nuestra función generadora un callback, por ejemplo, en un forEach(), pero esa función no es una generadora y no podremos incluir ahí una instrucción yield. Por ejemplo, este código no funciona ya que yield no está realmente dentro de una función generadora:


```javascript
"use strict";

function asyncSqrt(value, callback) {
    console.log('START execution with value =', value);
    setTimeout(function() {
        callback(value, value * value);
    }, 0 | Math.random() * 100);
}

function* gen(callback) {
    var arr = [0,1,2,3,4,5,6,7,8,9];
    arr.forEach(function (i) {
        yield asyncSqrt(i, callback);
    });
}
var iterator = gen(function (value, result) {
    console.log('END execution with value =', value, 'and result =', result);
    iterator.next();
});
iterator.next();
```
Dando este error en la ejecución:

```shell
$ node asyncGeneradorasBad.js
/home/user/workspace/laboratorios/javascript_lab/node_lab/js_asincronia/1_Async/asyncGeneradorasBad.js:13
        yield asyncSqrt(i, callback);
        ^^^^^
SyntaxError: Unexpected strict mode reserved word
    at Object.exports.runInThisContext (vm.js:76:16)
    at Module._compile (module.js:542:28)
    at Object.Module._extensions..js (module.js:579:10)
    at Module.load (module.js:487:32)
    at tryModuleLoad (module.js:446:12)
    at Function.Module._load (module.js:438:3)
    at Module.runMain (module.js:604:10)
    at run (bootstrap_node.js:394:7)
    at startup (bootstrap_node.js:149:9)
    at bootstrap_node.js:509:3
```

## Librería Co

Podemos usar esta librería para crear corrutinas:

Aunque el modelo basado sólo en una función generadora funciona, lo cierto es que tenemos que encargarnos de llamar sucesivamente a next() y hace que el sistema se complique un poco. Para solucionar esta complicación y hacernos las vida más sencilla tenemos un poderoso aliado: co.
Antes de adentrarnos en co() vamos a ver un implementación mínima de este tipo de corrutinas y que hemos denominado myCo(). Como se puede observar lo que hacemos es llamar next() sucesivamente y obteniendo las promesas y obteniendo el resultado y volviendo a llamar al siguiente next().


```javascript
"use strict";

function promiseSqrt(value){
    return new Promise(function (fulfill, reject){
        console.log('START execution with value =', value);
        setTimeout(function() {
            fulfill({ value: value, result: value * value });
        }, 0 | Math.random() * 100);
    });
}

function myCo(gen) {
    var i = gen();
    function sequent(result) {
        var ret = i.next(result);
        if (!ret.done) {
            ret.value.then(sequent);
        }
    }
    sequent();
}

myCo(function* gen() {
    for (var n = 0; n <= 9; n++) {
        var obj = yield promiseSqrt(n);
        console.log('END execution with value =', obj.value, 'and result =', obj.result);
    }
});
```

Aunque pueda parecer que estamos haciendo algo raro o misterioso, lo cierto es bastante sencillo de comprender. El secreto está en que en un código del tipo var obj = yield promiseSqrt(n); el valor que se incluye en obj es realmente el valor que se devuelve en el siguiente next() y si esa llamada no tienen ningún valor, entonces se incluye el valor devuelto por promiseSqrt(n). Es decir, al llamar al anterior next() se obtiene una promesa y cuando esta es resuelta, se llama al siguiente next() incluyendo su resultado como parámetro para que sea utilizado como asignación en el valor a la izquierda del yield.
Nuestro pequeño ejemplo es funcional, pero no gestiona los errores, no es capaz de trabajar con matrices de promesas u otros tipos de datos y desde luego no es un código muy probado. Para realizar una gestión completa podemos utilizar co(), una popular librería que nos permite controlar la ejecución asíncrona de forma muy sencilla, lo cual hace que nuestro código sea realmente compacto y fácil de leer. Vamos a ver nuestro ejemplo en NodeJS con la librería co:

Usando la librería de co:

```javascript
"use strict";

const co = require('co');

function promiseSqrt(value){
    return new Promise(function (fulfill, reject){
        console.log('START execution with value =', value);
        setTimeout(function() {
            fulfill({ value: value, result: value * value });
        }, 0 | Math.random() * 100);
    });
}

co(function* () {
    for (var n = 0; n <= 9; n++) {
        var obj = yield promiseSqrt(n);
        console.log('END execution with value =', obj.value, 'and result =', obj.result);
    }
});
```

Esta librería co() no sólo funciona con promesas, si no que también puede trabajar con otros elementos donde ubiquemos antes un yield:
 -  Promesas
 -  thunks (funciones)
 -  array (ejecución en paralelo de los elementos)
 -  objetos (ejecución en paralelo de los miembros)
 -  generadores (delegación)
 -  funciones generadoras (delegación)

En ocasiones es posible que no podamos utilizar un bucle para hacer las llamadas a las diferentes funciones asíncronas y tengamos que situar una detrás de la otra para hacer todo lo que necesitamos. En este caso el código con co() queda bastante claro de leer:

```javascript
"use strict";
const co = require('co');

function promiseSqrt(value){
    return new Promise(function (fulfill, reject){
        console.log('START execution with value =', value);
        setTimeout(function() {
            fulfill({ value: value, result: value * value });
        }, 0 | Math.random() * 100);
    });
}

co(function* () {
    var obj = yield promiseSqrt(0);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = yield promiseSqrt(1);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = yield promiseSqrt(2);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = yield promiseSqrt(3);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = yield promiseSqrt(4);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = yield promiseSqrt(5);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = yield promiseSqrt(6);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = yield promiseSqrt(7);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = yield promiseSqrt(8);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = yield promiseSqrt(9);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = yield promiseSqrt(10);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
});
```

Aunque nuestro ejemplo no tiene mucha utilidad, podemos ver como podemos situar una tras otras las instrucciones yield siguiendo un orden de ejecución que nos recuerda mucho a la ejecución síncrona, aunque realmente se esté realizando una ejecución asíncrona cada vez que aparece un yield.

## Thunks y co

Especialmente interesante es el uso de co() con los thunk, es decir, una función que permite el paso de un modelo o framework a otro. En nuestro caso el thunk se utiliza para pasar de un modelo basado en callbacks a un modelo que pueda ser utilizado por co() en una instrucción yield. Para ello tenemos que hacer un par de cosas.

1) En primer lugar nuestro callback debe utilizar el patrón típico de NodeJS: callback(err, value1, [value2], ...), es decir, el primer parámetro debe ser el código de error o null, seguido del resto de valores. Esta convención está muy extendida y no será un problema, aunque en nuestro pequeño ejemplo deberemos cambiar la forma de llamar al callback, ya que no gestionamos ningún tipo de error.

2) En segundo lugar debemos crear una función que realice una ejecución parcial con los parámetros de entrada y devuelva una función que sólo reciba el callback. Podemos usar una librería como thunkify, pero como venimos haciendo, lo haremos nosotros mismos para entender su funcionamiento:

```javascript
"use strict";

const co = require('co');

function asyncSqrt(value, callback) {
    console.log('START execution with value =', value);
    setTimeout(function() {
        callback(null, {value, result: value * value});
    }, 0 | Math.random() * 100);
}

const thunkAsyncSqrt = function(value) {
    return function(callback) {
        asyncSqrt(value, callback);
    };
};

co(function* () {
    for (var n = 0; n <= 9; n++) {
        var obj = yield thunkAsyncSqrt(n);
        console.log('END execution with value =', obj.value, 'and result =', obj.result);
    }
});
```

## El futuro: async y await

Siguiendo el modelo de co() para controlar la ejecución asíncrona en Javascript, en breve dispondremos de las instrucciones async y await. Han sido propuestas por Brian Terlson para ser parte del estándar ECMAScript y que se encuentran en un avanzado estado de desarrollo en muchos entornos (de momento están disponibles de forma nativa en Edge 13 con un flag y Node-Chakracore, aunque podemos empezar a comprobar su funcionamiento con __Babel__).

```javascript
"use strict";

function promiseSqrt(value){
    return new Promise(function (fulfill, reject){
        console.log('START execution with value =', value);
        setTimeout(function() {
            fulfill({ value: value, result: value * value });
        }, 0 | Math.random() * 100);
    });
}

async function run() {
    for (var n = 0; n <= 9; n++) {
        var obj = await promiseSqrt(n);
        console.log('END execution with value =', obj.value, 'and result =', obj.result);
    }
}
run();
```

La verdad es que se parece mucho al uso de co(), aunque con algunas diferencias:

  - En vez de co(function* ... utilizaremos async function...
  - En vez de yield utilizaremos await
  - Tendremos que iniciar la ejecución de la función asíncrona, ya que co() la ejecuta directamente y en este caso tendremos que hacerlo nosotros, aunque realmente si quieremos el mismo comportamiento podemos usar co.wrap().

Si utilizamos varias instrucciones await segundas unas de otras, nuevamente obtenemos un código con apariencia secuencia y que nos resulta sencillo de leer y comprender, aunque la ejecución de las funciones sea asíncrona.

```javascript
"use strict";

function promiseSqrt(value){
    return new Promise(function (fulfill, reject){
        console.log('START execution with value =', value);
        setTimeout(function() {
            fulfill({ value: value, result: value * value });
        }, 0 | Math.random() * 100);
    });
}

(async function () {
    var obj = await promiseSqrt(0);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = await promiseSqrt(1);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = await promiseSqrt(2);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = await promiseSqrt(3);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = await promiseSqrt(4);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = await promiseSqrt(5);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = await promiseSqrt(6);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = await promiseSqrt(7);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = await promiseSqrt(8);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = await promiseSqrt(9);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = await promiseSqrt(10);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
})();
```

async devuelve una promesa, por lo que podemos anidar las llamadas a funciones asíncronas, además de gestionar los errores que se puedan producir en un catch() u obtener el resultado final en un then().

async/await se pueden utilizar con funciones generales y funciones flecha, en breve será parte del lenguaje y no requieren ningún tipo de librería externa, por lo que son un futuro muy interesante.

## Convertir callback en promesas

Parece ser que await no funciona con un thunk del estilo que utilizamos en co(), pero tenemos una alternativa sencilla, convertir las funciones que utilizan callback en funciones que devuelven promesas. Para ello podemos utilizar una librería como pify que convierte en promesas cualquier función que utilice un callback con la convención tipo NodeJS.
Veamos nuestro ejemplo convirtiendo la función basada en callback a promesas con pify().

```javascript
"use strict";

const pify = require('pify');

function asyncSqrt(value, callback) {
    console.log('START execution with value =', value);
    setTimeout(function() {
        callback(null, {value: value, result: value * value});
    }, 0 | Math.random() * 100);
}
const promiseSqrt = pify(asyncSqrt);

(async function () {
    for (var n = 0; n <= 9; n++) {
        var obj = await promiseSqrt(n);
        console.log('END execution with value =', obj.value, 'and result =', obj.result);
    }
})();
```
