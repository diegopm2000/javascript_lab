"use strict";

function promiseSqrt(value){
    return new Promise(function (fulfill, reject){
        console.log('START execution with value =', value);
        setTimeout(function() {
            fulfill({ value: value, result: value * value });
        }, 0 | Math.random() * 100);
    });
}

(async function () {
    var obj = await promiseSqrt(0);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = await promiseSqrt(1);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = await promiseSqrt(2);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = await promiseSqrt(3);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = await promiseSqrt(4);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = await promiseSqrt(5);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = await promiseSqrt(6);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = await promiseSqrt(7);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = await promiseSqrt(8);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = await promiseSqrt(9);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = await promiseSqrt(10);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
})();
