"use strict";

function promiseSqrt(value){
    return new Promise(function (fulfill, reject){
        console.log('START execution with value =', value);
        setTimeout(function() {
            fulfill({ value: value, result: value * value });
        }, 0 | Math.random() * 100);
    });
}

promiseSqrt(0).then(function(obj) {
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    return promiseSqrt(1);
}).then(function(obj) {
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    return promiseSqrt(2);
}).then(function(obj) {
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    return promiseSqrt(3);
}).then(function(obj) {
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    return promiseSqrt(4);
}).then(function(obj) {
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    return promiseSqrt(5);
}).then(function(obj) {
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    return promiseSqrt(6);
}).then(function(obj) {
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    return promiseSqrt(7);
}).then(function(obj) {
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    return promiseSqrt(8);
}).then(function(obj) {
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    return promiseSqrt(9);
}).then(function(obj) {
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
}).catch(function(err) {
    console.error(err);
});
