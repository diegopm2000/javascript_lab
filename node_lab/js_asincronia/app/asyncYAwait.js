"use strict";

function promiseSqrt(value){
    return new Promise(function (fulfill, reject){
        console.log('START execution with value =', value);
        setTimeout(function() {
            fulfill({ value: value, result: value * value });
        }, 0 | Math.random() * 100);
    });
}

async function run() {
    for (var n = 0; n <= 9; n++) {
        var obj = await promiseSqrt(n);
        console.log('END execution with value =', obj.value, 'and result =', obj.result);
    }
}
run();
