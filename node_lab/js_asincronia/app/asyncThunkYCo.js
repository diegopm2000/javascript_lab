"use strict";

const co = require('co');

function asyncSqrt(value, callback) {
    console.log('START execution with value =', value);
    setTimeout(function() {
        callback(null, {value, result: value * value});
    }, 0 | Math.random() * 100);
}

const thunkAsyncSqrt = function(value) {
    return function(callback) {
        asyncSqrt(value, callback);
    };
};

co(function* () {
    for (var n = 0; n <= 9; n++) {
        var obj = yield thunkAsyncSqrt(n);
        console.log('END execution with value =', obj.value, 'and result =', obj.result);
    }
});
