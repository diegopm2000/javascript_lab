"use strict";
const co = require('co');

function promiseSqrt(value){
    return new Promise(function (fulfill, reject){
        console.log('START execution with value =', value);
        setTimeout(function() {
            fulfill({ value: value, result: value * value });
        }, 0 | Math.random() * 100);
    });
}

co(function* () {
    var obj = yield promiseSqrt(0);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = yield promiseSqrt(1);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = yield promiseSqrt(2);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = yield promiseSqrt(3);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = yield promiseSqrt(4);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = yield promiseSqrt(5);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = yield promiseSqrt(6);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = yield promiseSqrt(7);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = yield promiseSqrt(8);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = yield promiseSqrt(9);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
    obj = yield promiseSqrt(10);
    console.log('END execution with value =', obj.value, 'and result =', obj.result);
});
