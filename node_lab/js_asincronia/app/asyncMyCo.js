"use strict";

function promiseSqrt(value){
    return new Promise(function (fulfill, reject){
        console.log('START execution with value =', value);
        setTimeout(function() {
            fulfill({ value: value, result: value * value });
        }, 0 | Math.random() * 100);
    });
}

function myCo(gen) {
    var i = gen();
    function sequent(result) {
        var ret = i.next(result);
        if (!ret.done) {
            ret.value.then(sequent);
        }
    }
    sequent();
}

myCo(function* gen() {
    for (var n = 0; n <= 9; n++) {
        var obj = yield promiseSqrt(n);
        console.log('END execution with value =', obj.value, 'and result =', obj.result);
    }
});
