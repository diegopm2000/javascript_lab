"use strict";

const pify = require('pify');

function asyncSqrt(value, callback) {
    console.log('START execution with value =', value);
    setTimeout(function() {
        callback(null, {value: value, result: value * value});
    }, 0 | Math.random() * 100);
}
const promiseSqrt = pify(asyncSqrt);

(async function () {
    for (var n = 0; n <= 9; n++) {
        var obj = await promiseSqrt(n);
        console.log('END execution with value =', obj.value, 'and result =', obj.result);
    }
})();
