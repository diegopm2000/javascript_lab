var expect    = require("chai").expect;
var calculator = require("../app/calculator");

describe("Calculator", function() {

  describe("Add function", function() {
    it("add two numbers", function() {
      var result = calculator.add(2,2);

      expect(2+2).to.equal(result);
    });
  });

  describe("Substract function", function() {
    it("substract one number to other", function() {
      var result = calculator.substract(2,2);

      expect(2-2).to.equal(result);
    });
  });

  describe("Multiply function", function() {
    it("multiply two numbers", function() {
      var result = calculator.multiply(2,2);

      expect(2*2).to.equal(result);
    });
  });

  describe("Divide function", function() {
    it("divide one number between other", function() {
      var result = calculator.divide(2,2);

      expect(2/2).to.equal(result);
    });
  });

  describe("Divide function by zero", function() {
    it("divide one number between zero", function() {
      var result = calculator.divide(2,0);

      expect(2/0).to.equal(result);
    });
  });

});
