# Laboratorio de Sequelize

Sequelize es un ORM para javascript, que soporta diversos motores de bases de datos relacionales.

La página web principal del producto está aquí:

http://docs.sequelizejs.com/manual/installation/getting-started.html

En este laboratorio básico, se han definido dos tablas: Una para Roles y otra para usuarios.

Existiendo una relacion 1:N entre la tabla Roles y la tabla Usuarios (un rol puede estar relacionado con N usuarios)

En el fichero principal index.js, se ha realizado lo siguiente:

- Conexión a la base de datos
- Definición del modelo de datos
- Creación de las tablas en la base de datos
- Inserción de algunos valores de prueba
- Ejecución de algunas consultas de prueba
