var expect    = require("chai").expect;
var TileStatus = require("../app/TileStatus");
var Tile = require("../app/Tile");
var Board = require("../app/Board");

describe("TicTacToe - BoardTest", function() {

  describe("Creation of a new Board", function() {
    it("Creates a new Board with all tiles on EMPTY status", function() {

      var myBoard = new Board();

      var bResult = true;

      for (var i in myBoard.arrayBoard) {
        for (var j in myBoard.arrayBoard) {
          bResult = bResult && myBoard.getTile(i, j).isEmpty();
        }
      }

      console.log(myBoard.toString());
      expect(true).to.equal(bResult);
    });
  });


  describe("Settings Tiles...", function() {
    it("Set EMPTY into all tiles of the board", function() {

      var myBoard = new Board();

      var bResult = true;

      for (var i in myBoard.arrayBoard) {
        for (var j in myBoard.arrayBoard) {
          myBoard.setTile(i, j, TileStatus.EMPTY);
          bResult = bResult && myBoard.getTile(i, j).isEmpty();
        }
      }

      expect(true).to.equal(bResult);
    });
  });

  describe("Settings Tiles...", function() {

    it("Set X into all tiles of the board", function() {

      var myBoard = new Board();

      var bResult = true;

      for (var i in myBoard.arrayBoard) {
        for (var j in myBoard.arrayBoard) {
          myBoard.setTile(i, j, TileStatus.X);
          bResult = bResult && myBoard.getTile(i, j).isX();
        }
      }

      expect(true).to.equal(bResult);
    });

    it("Set O into all tiles of the board", function() {

      var myBoard = new Board();

      var bResult = true;

      for (var i in myBoard.arrayBoard) {
        for (var j in myBoard.arrayBoard) {
          myBoard.setTile(i, j, TileStatus.O);
          bResult = bResult && myBoard.getTile(i, j).isO();
        }
      }

      expect(true).to.equal(bResult);
    });
  });

  describe("Searching X row", function() {

    it("Searching X row in the first row", function() {

      var myBoard = new Board();

      myBoard.setTile(0, 0, TileStatus.X);
      myBoard.setTile(0, 1, TileStatus.X);
      myBoard.setTile(0, 2, TileStatus.X);

      var bResult = myBoard.isRowStatus(TileStatus.X);

      expect(true).to.equal(bResult);
    });

    it("Searching X row in the second row", function() {

      var myBoard = new Board();

      myBoard.setTile(1, 0, TileStatus.X);
      myBoard.setTile(1, 1, TileStatus.X);
      myBoard.setTile(1, 2, TileStatus.X);

      var bResult = myBoard.isRowStatus(TileStatus.X);

      expect(true).to.equal(bResult);
    });

    it("Searching X row in the third row", function() {

      var myBoard = new Board();

      myBoard.setTile(2, 0, TileStatus.X);
      myBoard.setTile(2, 1, TileStatus.X);
      myBoard.setTile(2, 2, TileStatus.X);

      var bResult = myBoard.isRowStatus(TileStatus.X);

      expect(true).to.equal(bResult);
    });
  });

  describe("Searching O row", function() {
    it("Searching O  row in the first row", function() {

      var myBoard = new Board();

      myBoard.setTile(0, 0, TileStatus.O);
      myBoard.setTile(0, 1, TileStatus.O);
      myBoard.setTile(0, 2, TileStatus.O);

      var bResult = myBoard.isRowStatus(TileStatus.O);

      expect(true).to.equal(bResult);
    });

    it("Searching O  row in the second row", function() {

      var myBoard = new Board();

      myBoard.setTile(1, 0, TileStatus.O);
      myBoard.setTile(1, 1, TileStatus.O);
      myBoard.setTile(1, 2, TileStatus.O);

      var bResult = myBoard.isRowStatus(TileStatus.O);

      expect(true).to.equal(bResult);
    });

    it("Searching O  row in the third row", function() {

      var myBoard = new Board();

      myBoard.setTile(2, 0, TileStatus.O);
      myBoard.setTile(2, 1, TileStatus.O);
      myBoard.setTile(2, 2, TileStatus.O);

      var bResult = myBoard.isRowStatus(TileStatus.O);

      expect(true).to.equal(bResult);
    });
  });

  describe("Searching X column", function() {
    it("Searching X column in the first row", function() {

      var myBoard = new Board();

      myBoard.setTile(0, 0, TileStatus.X);
      myBoard.setTile(1, 0, TileStatus.X);
      myBoard.setTile(2, 0, TileStatus.X);

      var bResult = myBoard.isColumnStatus(TileStatus.X);

      expect(true).to.equal(bResult);
    });

    it("Searching X column in the second row", function() {

      var myBoard = new Board();

      myBoard.setTile(0, 1, TileStatus.X);
      myBoard.setTile(1, 1, TileStatus.X);
      myBoard.setTile(2, 1, TileStatus.X);

      var bResult = myBoard.isColumnStatus(TileStatus.X);

      expect(true).to.equal(bResult);
    });

    it("Searching X column in the third row", function() {

      var myBoard = new Board();

      myBoard.setTile(0, 2, TileStatus.X);
      myBoard.setTile(1, 2, TileStatus.X);
      myBoard.setTile(2, 2, TileStatus.X);

      var bResult = myBoard.isColumnStatus(TileStatus.X);

      expect(true).to.equal(bResult);
    });
  });

  describe("Searching O column", function() {
    it("Searching O column in the first row", function() {

      var myBoard = new Board();

      myBoard.setTile(0, 0, TileStatus.O);
      myBoard.setTile(1, 0, TileStatus.O);
      myBoard.setTile(2, 0, TileStatus.O);

      var bResult = myBoard.isColumnStatus(TileStatus.O);

      expect(true).to.equal(bResult);
    });

    it("Searching O column in the second row", function() {

      var myBoard = new Board();

      myBoard.setTile(0, 1, TileStatus.O);
      myBoard.setTile(1, 1, TileStatus.O);
      myBoard.setTile(2, 1, TileStatus.O);

      var bResult = myBoard.isColumnStatus(TileStatus.O);

      expect(true).to.equal(bResult);
    });

    it("Searching O column in the third row", function() {

      var myBoard = new Board();

      myBoard.setTile(0, 2, TileStatus.O);
      myBoard.setTile(1, 2, TileStatus.O);
      myBoard.setTile(2, 2, TileStatus.O);

      var bResult = myBoard.isColumnStatus(TileStatus.O);

      expect(true).to.equal(bResult);
    });
  });

  describe("Searching first X diagonal", function() {
    it("Searching first X diagonal", function() {

      var myBoard = new Board();

      myBoard.setTile(0, 0, TileStatus.X);
      myBoard.setTile(1, 1, TileStatus.X);
      myBoard.setTile(2, 2, TileStatus.X);

      var bResult = myBoard.isDiagonalStatus(TileStatus.X);

      expect(true).to.equal(bResult);
    });
  });

  describe("Searching first O diagonal", function() {
    it("Searching first O diagonal", function() {

      var myBoard = new Board();

      myBoard.setTile(0, 0, TileStatus.O);
      myBoard.setTile(1, 1, TileStatus.O);
      myBoard.setTile(2, 2, TileStatus.O);

      var bResult = myBoard.isDiagonalStatus(TileStatus.O);

      expect(true).to.equal(bResult);
    });
  });

  describe("Searching second X diagonal", function() {
    it("Searching second X diagonal", function() {

      var myBoard = new Board();

      myBoard.setTile(2, 0, TileStatus.X);
      myBoard.setTile(1, 1, TileStatus.X);
      myBoard.setTile(0, 2, TileStatus.X);

      var bResult = myBoard.isDiagonalStatus(TileStatus.X);

      expect(true).to.equal(bResult);
    });
  });

  describe("Searching second O diagonal", function() {
    it("Searching second O diagonal", function() {

      var myBoard = new Board();

      myBoard.setTile(2, 0, TileStatus.O);
      myBoard.setTile(1, 1, TileStatus.O);
      myBoard.setTile(0, 2, TileStatus.O);

      var bResult = myBoard.isDiagonalStatus(TileStatus.O);

      expect(true).to.equal(bResult);
    });
  });

  describe("Searching X wins the game", function() {
    it("Searching X wins the game", function() {

      var myBoard = new Board();
      myBoard.setTile(0, 0, TileStatus.X);
      myBoard.setTile(0, 1, TileStatus.X);
      myBoard.setTile(0, 2, TileStatus.X);
      var bResult = myBoard.isXWin();
      expect(true).to.equal(bResult);

      myBoard = new Board();
      myBoard.setTile(1, 0, TileStatus.X);
      myBoard.setTile(1, 1, TileStatus.X);
      myBoard.setTile(1, 2, TileStatus.X);
      var bResult = myBoard.isXWin();
      expect(true).to.equal(bResult);

      myBoard = new Board();
      myBoard.setTile(2, 0, TileStatus.X);
      myBoard.setTile(2, 1, TileStatus.X);
      myBoard.setTile(2, 2, TileStatus.X);
      var bResult = myBoard.isXWin();
      expect(true).to.equal(bResult);

      myBoard = new Board();
      myBoard.setTile(0, 0, TileStatus.X);
      myBoard.setTile(1, 0, TileStatus.X);
      myBoard.setTile(2, 0, TileStatus.X);
      var bResult = myBoard.isXWin();
      expect(true).to.equal(bResult);

      myBoard = new Board();
      myBoard.setTile(0, 1, TileStatus.X);
      myBoard.setTile(1, 1, TileStatus.X);
      myBoard.setTile(2, 1, TileStatus.X);
      var bResult = myBoard.isXWin();
      expect(true).to.equal(bResult);

      myBoard = new Board();
      myBoard.setTile(0, 2, TileStatus.X);
      myBoard.setTile(1, 2, TileStatus.X);
      myBoard.setTile(2, 2, TileStatus.X);
      var bResult = myBoard.isXWin();
      expect(true).to.equal(bResult);

      myBoard = new Board();
      myBoard.setTile(0, 0, TileStatus.X);
      myBoard.setTile(1, 1, TileStatus.X);
      myBoard.setTile(2, 2, TileStatus.X);
      var bResult = myBoard.isXWin();
      expect(true).to.equal(bResult);

      myBoard = new Board();
      myBoard.setTile(0, 2, TileStatus.X);
      myBoard.setTile(1, 1, TileStatus.X);
      myBoard.setTile(2, 0, TileStatus.X);
      var bResult = myBoard.isXWin();
      expect(true).to.equal(bResult);

    });
  });

  describe("Searching O wins the game", function() {
    it("Searching O wins the game", function() {

      var myBoard = new Board();
      myBoard.setTile(0, 0, TileStatus.O);
      myBoard.setTile(0, 1, TileStatus.O);
      myBoard.setTile(0, 2, TileStatus.O);
      var bResult = myBoard.isOWin();
      expect(true).to.equal(bResult);

      myBoard = new Board();
      myBoard.setTile(1, 0, TileStatus.O);
      myBoard.setTile(1, 1, TileStatus.O);
      myBoard.setTile(1, 2, TileStatus.O);
      var bResult = myBoard.isOWin();
      expect(true).to.equal(bResult);

      myBoard = new Board();
      myBoard.setTile(2, 0, TileStatus.O);
      myBoard.setTile(2, 1, TileStatus.O);
      myBoard.setTile(2, 2, TileStatus.O);
      var bResult = myBoard.isOWin();
      expect(true).to.equal(bResult);

      myBoard = new Board();
      myBoard.setTile(0, 0, TileStatus.O);
      myBoard.setTile(1, 0, TileStatus.O);
      myBoard.setTile(2, 0, TileStatus.O);
      var bResult = myBoard.isOWin();
      expect(true).to.equal(bResult);

      myBoard = new Board();
      myBoard.setTile(0, 1, TileStatus.O);
      myBoard.setTile(1, 1, TileStatus.O);
      myBoard.setTile(2, 1, TileStatus.O);
      var bResult = myBoard.isOWin();
      expect(true).to.equal(bResult);

      myBoard = new Board();
      myBoard.setTile(0, 2, TileStatus.O);
      myBoard.setTile(1, 2, TileStatus.O);
      myBoard.setTile(2, 2, TileStatus.O);
      var bResult = myBoard.isOWin();
      expect(true).to.equal(bResult);

      myBoard = new Board();
      myBoard.setTile(0, 0, TileStatus.O);
      myBoard.setTile(1, 1, TileStatus.O);
      myBoard.setTile(2, 2, TileStatus.O);
      var bResult = myBoard.isOWin();
      expect(true).to.equal(bResult);

      myBoard = new Board();
      myBoard.setTile(0, 2, TileStatus.O);
      myBoard.setTile(1, 1, TileStatus.O);
      myBoard.setTile(2, 0, TileStatus.O);
      var bResult = myBoard.isOWin();
      expect(true).to.equal(bResult);
    });
  });

  describe("Searching draw game", function() {
    it("Searching draw game", function() {

      var myBoard = new Board();
      myBoard.setTile(0, 0, TileStatus.X);
      myBoard.setTile(0, 1, TileStatus.O);
      myBoard.setTile(0, 2, TileStatus.O);
      myBoard.setTile(1, 0, TileStatus.O);
      myBoard.setTile(1, 1, TileStatus.O);
      myBoard.setTile(1, 2, TileStatus.X);
      myBoard.setTile(2, 0, TileStatus.X);
      myBoard.setTile(2, 1, TileStatus.X);
      myBoard.setTile(2, 2, TileStatus.O);

      var bResult = myBoard.isDraw();
      expect(true).to.equal(bResult);

    });
  });

});
