var expect    = require("chai").expect;
var TileStatus = require("../app/TileStatus");
var Tile = require("../app/Tile");

describe("TicTacToe - TileTest", function() {

  describe("Creation of a new Tile", function() {

    it("Creates a new Tile with status EMPTY by default", function() {

      var myTile = new Tile();

      expect(myTile.isEmpty()).to.equal(true);
    });

    it("Creates a new Tile and set status to EMPTY", function() {

      var myTile = new Tile();
      myTile.status = TileStatus.EMPTY;

      expect(myTile.isEmpty()).to.equal(true);
    });

    it("Creates a new Tile and set status to O", function() {

      var myTile = new Tile();
      myTile.status = TileStatus.O;

      expect(myTile.isO()).to.equal(true);
    });

    it("Creates a new Tile and set status to X", function() {

      var myTile = new Tile();
      myTile.status = TileStatus.X;

      expect(myTile.isX()).to.equal(true);
    });

  });
  
});
