var expect    = require("chai").expect;
var Player = require("../app/Player");

describe("TicTacToe - TileTest", function() {

  describe("Creation of a new Player", function() {
    it("Creates a new Player with params", function() {

      var myPlayer = new Player(1, "name", "nickname");

      expect(myPlayer.getId()).to.equal(1);
      expect(myPlayer.getName()).to.equal("name");
      expect(myPlayer.getNickname()).to.equal("nickname");
    });
  });

  describe("Modifications of properties of Player", function() {
    it("Creates a new Player with params and then modifies his properties", function() {

      var myPlayer = new Player(1, "name", "nickname");

      myPlayer.setId(2);
      myPlayer.setName("namealt");
      myPlayer.setNickname("nicknamealt");

      expect(myPlayer.getId()).to.equal(2);
      expect(myPlayer.getName()).to.equal("namealt");
      expect(myPlayer.getNickname()).to.equal("nicknamealt");
    });
  });

});
