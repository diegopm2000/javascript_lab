var expect    = require("chai").expect;
var assert    = require("chai").assert;
var Board = require("../app/Board");
var Player = require("../app/Player");
var Game = require("../app/Game");
var MovValidationResult = require("../app/MovValidationResult");
var GameStatus = require("../app/GameStatus");

describe("TicTacToe - BoardTest", function() {

  var player1;
  var player2;
  var game;

  beforeEach(function() {

    player1 = new Player(1, "nameX", "nicknameX");
    player2 = new Player(2, "nameO", "nicknameO");
    game = new Game(player1, player2);
  });

  describe("Creating a new game", function() {
    it("Creates a new game succesfully", function() {

      expect(game.getLastValidation()).to.equal(null);
      expect(game.getGameStatus()).to.equal(GameStatus.XTURN);
      expect(game.getPlayerX()).to.equal(player1);
      expect(game.getPlayerO()).to.equal(player2);
      expect(game.getBoard().isEmpty()).to.equal(true);

      console.log(game.toString());

    });
  });

  describe("Checking players are on game", function() {
    it("Checks if the correct players are on game", function() {

      var myPlayerAlt = new Player(3, "nameAlt", "nicknameAlt");

      assert.equal(game.isPlayerX(player1) || game.isPlayerO(player1), true);
      assert.equal(game.isPlayerX(player2) || game.isPlayerO(player2), true);
      assert.notEqual(game.isPlayerX(myPlayerAlt) || game.isPlayerO(myPlayerAlt), true);

    });

    it("Checking players are assigned correctly", function() {

      assert.equal(game.isPlayerX(player1), true);
      assert.equal(game.isPlayerO(player2), true);
    });
  });

  describe("Check player X won", function() {

    it("Checks if player X won in first row", function() {

      game.move(player1, 0, 0);
  		game.move(player2, 1, 0);
  		game.move(player1, 0, 1);
  		game.move(player2, 2, 0);
  		game.move(player1, 0, 2);

      console.log(game.toString());

      assert.equal(game.isXWins(), true);

    });

    it("Checks if player X won in second row", function() {

      game.move(player1, 1, 0);
      game.move(player2, 0, 0);
      game.move(player1, 1, 1);
      game.move(player2, 0, 1);
      game.move(player1, 1, 2);

      console.log(game.toString());

      assert.equal(game.isXWins(), true);

    });

    it("Checks if player X won in third row", function() {

      game.move(player1, 2, 0);
    	game.move(player2, 0, 0);
    	game.move(player1, 2, 1);
    	game.move(player2, 0, 1);
    	game.move(player1, 2, 2);

      console.log(game.toString());

      assert.equal(game.isXWins(), true);

    });

    it("Checks if player X won in diagonal A", function() {

      game.move(player1, 0, 0);
  		game.move(player2, 1, 0);
  		game.move(player1, 1, 1);
  		game.move(player2, 1, 2);
  		game.move(player1, 2, 2);

      console.log(game.toString());

      assert.equal(game.isXWins(), true);

    });

    it("Checks if player X won in diagonal B", function() {

      game.move(player1, 0, 2);
  		game.move(player2, 1, 0);
  		game.move(player1, 1, 1);
  		game.move(player2, 1, 2);
  		game.move(player1, 2, 0);

      console.log(game.toString());

      assert.equal(game.isXWins(), true);

    });

    it("Checks if player X won in first column", function() {

      game.move(player1, 0, 0);
  		game.move(player2, 1, 1);
  		game.move(player1, 1, 0);
  		game.move(player2, 2, 2);
  		game.move(player1, 2, 0);

      console.log(game.toString());

      assert.equal(game.isXWins(), true);

    });

    it("Checks if player X won in second column", function() {

      game.move(player1, 0, 1);
  		game.move(player2, 2, 2);
  		game.move(player1, 1, 1);
  		game.move(player2, 2, 0);
  		game.move(player1, 2, 1);

      console.log(game.toString());

      assert.equal(game.isXWins(), true);

    });

    it("Checks if player X won in third column", function() {

      game.move(player1, 0, 2);
  		game.move(player2, 2, 1);
  		game.move(player1, 1, 2);
  		game.move(player2, 2, 0);
  		game.move(player1, 2, 2);

      console.log(game.toString());

      assert.equal(game.isXWins(), true);

    });
  });

  describe("Check player O won", function() {

    it("Checks if player O won in first row", function() {

      game.move(player1, 1, 0);
  		game.move(player2, 0, 0);
  		game.move(player1, 1, 1);
  		game.move(player2, 0, 1);
  		game.move(player1, 2, 2);
  		game.move(player2, 0, 2);

      console.log(game.toString());

      assert.equal(game.isOWins(), true);

    });

    it("Checks if player O won in second row", function() {

      game.move(player1, 0, 0);
  		game.move(player2, 1, 0);
  		game.move(player1, 0, 1);
  		game.move(player2, 1, 1);
  		game.move(player1, 2, 2);
  		game.move(player2, 1, 2);

      console.log(game.toString());

      assert.equal(game.isOWins(), true);

    });

    it("Checks if player O won in third row", function() {

      game.move(player1, 1, 0);
  		game.move(player2, 2, 0);
  		game.move(player1, 1, 1);
  		game.move(player2, 2, 1);
  		game.move(player1, 0, 2);
  		game.move(player2, 2, 2);

      console.log(game.toString());

      assert.equal(game.isOWins(), true);

    });

    it("Checks if player O won in diagonal A", function() {

      game.move(player1, 0, 1);
  		game.move(player2, 0, 0);
  		game.move(player1, 1, 2);
  		game.move(player2, 1, 1);
  		game.move(player1, 0, 2);
  		game.move(player2, 2, 2);

      console.log(game.toString());

      assert.equal(game.isOWins(), true);

    });

    it("Checks if player O won in diagonal B", function() {

      game.move(player1, 0, 1);
  		game.move(player2, 0, 2);
  		game.move(player1, 1, 2);
  		game.move(player2, 1, 1);
  		game.move(player1, 0, 0);
  		game.move(player2, 2, 0);

      console.log(game.toString());

      assert.equal(game.isOWins(), true);

    });

    it("Checks if player O won in first column", function() {

      game.move(player1, 0, 1);
  		game.move(player2, 0, 0);
  		game.move(player1, 1, 1);
  		game.move(player2, 1, 0);
  		game.move(player1, 2, 2);
  		game.move(player2, 2, 0);

      console.log(game.toString());

      assert.equal(game.isOWins(), true);

    });

    it("Checks if player O won in second column", function() {

      game.move(player1, 1, 0);
  		game.move(player2, 0, 1);
  		game.move(player1, 2, 2);
  		game.move(player2, 1, 1);
  		game.move(player1, 0, 2);
  		game.move(player2, 2, 1);

      console.log(game.toString());

      assert.equal(game.isOWins(), true);

    });

    it("Checks if player O won in third column", function() {

      game.move(player1, 0, 1);
  		game.move(player2, 0, 2);
  		game.move(player1, 1, 1);
  		game.move(player2, 1, 2);
  		game.move(player1, 0, 0);
  		game.move(player2, 2, 2);

      console.log(game.toString());

      assert.equal(game.isOWins(), true);

    });
  });

  describe("Check player X won", function() {

    it("Checks if is a draw game", function() {

      game.move(player1, 0, 0);
  		game.move(player2, 0, 1);
  		game.move(player1, 1, 0);
  		game.move(player2, 2, 0);
  		game.move(player1, 1, 1);
  		game.move(player2, 2, 2);
  		game.move(player1, 0, 2);
  		game.move(player2, 1, 2);
  		game.move(player1, 2, 1);

      console.log(game.toString());

      assert.equal(game.isDraw(), true);
    });
  });

  describe("Checks complete game", function() {

    it("Player X finally won the game", function() {

      //1. First move of player 1

  		game.move(player1, 0, 0);
      console.log(game.toString());

      assert.equal(game.getGameStatus(), GameStatus.OTURN);
      assert.equal(game.getLastValidation(), MovValidationResult.OK);
      assert.equal(game.getBoard().getTile(0, 0).isX(), true);

      //2. Move of player 2

  		game.move(player2, 1, 1);
  		console.log(game.toString());

  		assert.equal(game.getGameStatus(), GameStatus.XTURN);
  		assert.equal(game.getLastValidation(), MovValidationResult.OK);
  		assert.equal(game.getBoard().getTile(1, 1).isO(), true);

      //3. Movement not valid of player 1

  		game.move(player1, 1, 1);
  		console.log(game.toString());

  		assert.equal(game.getGameStatus(), GameStatus.XTURN);
  		assert.equal(game.getLastValidation(), MovValidationResult.TILE_NOT_EMPTY);
  		assert.equal(game.getBoard().getTile(1, 1).isO(), true);

  		//4. Movement not valid of player 2 (not in turn)

  		game.move(player2, 1, 2);
  		console.log(game.toString());

  		assert.equal(game.getGameStatus(), GameStatus.XTURN);
  		assert.equal(game.getLastValidation(), MovValidationResult.PLAYER_NOT_IN_TURN);
  		assert.equal(game.getBoard().getTile(1, 2).isEmpty(), true);

      //5. Movement of player 1

  		game.move(player1, 0, 1);
  		console.log(game.toString());

  		assert.equal(game.getGameStatus(), GameStatus.OTURN);
  		assert.equal(game.getLastValidation(), MovValidationResult.OK);
  		assert.equal(game.getBoard().getTile(0, 1).isX(), true);

  		//6. Movement of player 2

  		game.move(player2, 0, 2);
  		console.log(game.toString());

  		assert.equal(game.getGameStatus(), GameStatus.XTURN);
  		assert.equal(game.getLastValidation(), MovValidationResult.OK);
  		assert.equal(game.getBoard().getTile(0, 2).isO(), true);

      //7. Movement of player 1

  		game.move(player1, 2, 0);
  		console.log(game.toString());

  		assert.equal(game.getGameStatus(), GameStatus.OTURN);
  		assert.equal(game.getLastValidation(), MovValidationResult.OK);
  		assert.equal(game.getBoard().getTile(2, 0).isX(), true);

  		//8. Movement of player 1 Not Valid (not in turn)

  		game.move(player1, 1, 0);
  		console.log(game.toString());

  		assert.equal(game.getGameStatus(), GameStatus.OTURN);
  		assert.equal(game.getLastValidation(), MovValidationResult.PLAYER_NOT_IN_TURN);
  		assert.equal(game.getBoard().getTile(1, 0).isEmpty(), true);

  		//9. Movement of player 2

  		game.move(player2, 2, 2);
  		console.log(game.toString());

  		assert.equal(game.getGameStatus(), GameStatus.XTURN);
  		assert.equal(game.getLastValidation(), MovValidationResult.OK);
  		assert.equal(game.getBoard().getTile(2, 2).isO(), true);

  		//10. Movement of player 1 (X Player Wins)

  		game.move(player1, 1, 0);
  		console.log(game.toString());

  		assert.equal(game.getGameStatus(), GameStatus.XWINS);
  		assert.equal(game.getLastValidation(), MovValidationResult.OK);
  		assert.equal(game.getBoard().getTile(1, 0).isX(), true);

    });
  });

});
