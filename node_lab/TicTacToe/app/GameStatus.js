//GameStatus.js

var GameStatus = Object.freeze({'OTURN':'OTURN', 'XTURN':'XTURN', 'XWINS':'XWINS', 'OWINS':'OWINS', 'DRAW':'DRAW'});

module.exports = GameStatus;
