//TileStatus.js

var TileStatus = Object.freeze({'EMPTY':'EMPTY', 'O':'O', 'X':'X'});

module.exports = TileStatus;
