//Player.js

function Player(id, name, nickname) {

  this.id = id;
  this.name = name;
  this.nickname = nickname;
}

Player.prototype.getId = function() {
  return this.id;
}

Player.prototype.setId = function(id) {
  return this.id = id;
}

Player.prototype.getName = function() {
  return this.name;
}

Player.prototype.setName = function(name) {
  return this.name = name;
}

Player.prototype.getNickname = function() {
  return this.nickname;
}

Player.prototype.setNickname = function(nickname) {
  return this.nickname = nickname;
}

Player.prototype.toString = function() {

  return 'Player: id='+this.id+', name='+this.name+', nickname='+this.nickname;
}

module.exports = Player;
