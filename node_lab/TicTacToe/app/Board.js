//Board.js

var TileStatus = require("../app/TileStatus");
var Tile = require("../app/Tile");

const MAX_ROWS = 3;
const MAX_COLUMNS = 3;

function Board() {

  this.arrayBoard = [
                    [new Tile(), new Tile(), new Tile()],
                    [new Tile(), new Tile(), new Tile()],
                    [new Tile(), new Tile(), new Tile()]
                  ];
}

Board.prototype.setTile = function(row, col, value) {
  this.arrayBoard[row][col].setValue(value);
}

Board.prototype.getTile = function(row, col) {
  return this.arrayBoard[row][col];
}

Board.prototype.isRowStatus = function(status) {
  return this.isNRowStatus(0,status) || this.isNRowStatus(1,status) || this.isNRowStatus(2,status);
}

Board.prototype.isNRowStatus = function(row, status) {
  return (this.getTile(row,0).isValue(status)) && (this.getTile(row,1).isValue(status)) && (this.getTile(row,2).isValue(status));
}

Board.prototype.isColumnStatus = function(status) {
  return this.isNColumnStatus(0,status) || this.isNColumnStatus(1,status) || this.isNColumnStatus(2,status);
}

Board.prototype.isNColumnStatus = function(column, status) {
  return (this.getTile(0,column).isValue(status)) && (this.getTile(1,column).isValue(status)) && (this.getTile(2,column).isValue(status));
}

Board.prototype.isDiagonalStatus = function(status) {
  return this.isFirstDiagonalStatus(status) || this.isSecondDiagonalStatus(status);
}

Board.prototype.isFirstDiagonalStatus = function(status) {
  return (this.getTile(0,0).isValue(status)) && (this.getTile(1,1).isValue(status)) && (this.getTile(2,2).isValue(status));
}

Board.prototype.isSecondDiagonalStatus = function(status) {
  return (this.getTile(0,2).isValue(status)) && (this.getTile(1,1).isValue(status)) && (this.getTile(2,0).isValue(status));
}

Board.prototype.isXWin = function() {
  return this.isStatusWin(TileStatus.X);
}

Board.prototype.isOWin = function() {
  return this.isStatusWin(TileStatus.O);
}

Board.prototype.isStatusWin = function(status) {
  return this.isRowStatus(status) || this.isColumnStatus(status) || this.isDiagonalStatus(status);
}

Board.prototype.isDraw = function() {
  return this.isBoardComplete() && (!this.isXWin()) && (!this.isOWin());
}

Board.prototype.isBoardComplete = function() {
  return this.isRowComplete(0) && this.isRowComplete(1) && this.isRowComplete(2);
}

Board.prototype.isRowComplete = function(row) {
  return (!this.getTile(row,0).isValue(TileStatus.EMPTY)) && (!this.getTile(row,1).isValue(TileStatus.EMPTY)) && (!this.getTile(row,2).isValue(TileStatus.EMPTY));
}

Board.prototype.getString = function() {
  return 'this is my board...';
}

Board.prototype.isEmpty = function() {
  return this.isNRowStatus(0, TileStatus.EMPTY) && this.isNRowStatus(1, TileStatus.EMPTY) && this.isNRowStatus(2, TileStatus.EMPTY)
}

Board.prototype.isEmptyTile = function(row, column) {
  return this.getTile(row,column).isEmpty();
}

Board.prototype.toString = function() {

  var sResult = '';

  for (var i=0; i<MAX_ROWS; i++) {
    for (var j=0; j<MAX_COLUMNS; j++) {
        sResult+= this.getTile(i,j);
    }
    if (i<MAX_ROWS-1) {
      sResult+='\n';
    }
  }

  return sResult;
}

module.exports = Board;
