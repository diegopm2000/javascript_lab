//MovValidationResult.js

var MovValidationResult = Object.freeze({'OK':'OK', 'PLAYER_NOT_IN_GAME':'PLAYER_NOT_IN_GAME', 'PLAYER_NOT_IN_TURN':'PLAYER_NOT_IN_TURN', 'TILE_NOT_EMPTY':'TILE_NOT_EMPTY'});

module.exports = MovValidationResult;
