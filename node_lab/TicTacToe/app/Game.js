//Game.js

var Board = require("../app/Board");
var GameStatus = require("../app/GameStatus");
var MovValidationResult = require("../app/MovValidationResult");

function Game(playerX, playerO) {

  this.playerX = playerX;
  this.playerO = playerO;
  this.board = new Board();
  this.gameStatus = GameStatus.XTURN;
  this.lastValidation = null;
}

Game.prototype.getPlayerX = function() {
  return this.playerX;
}

Game.prototype.getPlayerO = function() {
  return this.playerO;
}

Game.prototype.getBoard = function() {
  return this.board;
}

Game.prototype.getGameStatus = function() {
  return this.gameStatus;
}

Game.prototype.setGameStatus = function(status) {
  this.gameStatus = status;
}

Game.prototype.getLastValidation = function() {
  return this.lastValidation;
}

Game.prototype.isXTurn = function() {
  return this.gameStatus === GameStatus.XTURN;
}

Game.prototype.isOTurn = function() {
  return this.gameStatus === GameStatus.OTURN;
}

Game.prototype.isDraw = function() {
  return this.gameStatus === GameStatus.DRAW;
}

Game.prototype.isXWins = function() {
  return this.gameStatus === GameStatus.XWINS;
}

Game.prototype.isOWins = function() {
  return this.gameStatus === GameStatus.OWINS;
}

Game.prototype.isPlayerX = function(player) {
  return this.playerX.getId() === player.getId();
}

Game.prototype.isPlayerO = function(player) {
  return this.playerO.getId() === player.getId();
}

Game.prototype.isPlayerInGame = function(player) {
  return this.isPlayerX(player) || this.isPlayerO(player);
}

Game.prototype.isPlayerTurn = function(player) {
  return (this.isPlayerX(player) && this.isXTurn()) || (this.isPlayerO(player) && this.isOTurn());
}

Game.prototype.isEmptyTile = function(row, col) {
  return this.board.isEmptyTile(row, col);
}

Game.prototype.checkMove = function (player, row, col) {

  var movValidationResult;

  if (!this.isPlayerInGame(player)) {
    movValidationResult = MovValidationResult.PLAYER_NOT_IN_GAME;
  } else if (!this.isPlayerTurn(player)) {
    movValidationResult = MovValidationResult.PLAYER_NOT_IN_TURN;
  } else if (!this.isEmptyTile(row, col)) {
    movValidationResult = MovValidationResult.TILE_NOT_EMPTY;
  } else {
    movValidationResult = MovValidationResult.OK;
  }

  return movValidationResult;
}

Game.prototype.move = function(player, row, col) {

  this.lastValidation = this.checkMove(player, row, col);

  if (this.lastValidation === MovValidationResult.OK) {
    if (this.isPlayerX(player)) {

      this.board.getTile(row, col).setX();

      if (this.board.isXWin()) {
        this.setGameStatus(GameStatus.XWINS);
      } else if(this.board.isBoardComplete()) {
        this.setGameStatus(GameStatus.DRAW);
      } else {
        this.setGameStatus(GameStatus.OTURN);
      }
    } else {

      this.board.getTile(row, col).setO();

      if (this.board.isOWin()) {
        this.setGameStatus(GameStatus.OWINS);
      } else if(this.board.isBoardComplete()) {
        this.setGameStatus(GameStatus.DRAW);
      } else {
        this.setGameStatus(GameStatus.XTURN);
      }
    }
  } else {
    console.error("Bad movement");
  }

  return this;
}

Game.prototype.toString = function() {

  var sResult = '';

  sResult+='Game:\n';
  sResult+='PlayerX:'+this.playerX.toString()+'\n';
  sResult+='PlayerO:'+this.playerO.toString()+'\n';
  sResult+='Game Status:'+this.gameStatus+'\n';
  sResult+='Last Move Validation:'+this.lastValidation+'\n';
  sResult+=this.board.toString()+'\n';

  return sResult;
}

module.exports = Game;
