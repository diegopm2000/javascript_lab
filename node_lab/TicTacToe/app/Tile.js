//Tile.js

var TileStatus = require("../app/TileStatus");

function Tile() {

  this.status = TileStatus.EMPTY;
};

Tile.prototype.setValue = function(value) {
  this.status = value;
}

Tile.prototype.isValue = function(value) {
  return this.status === value;
}

Tile.prototype.setEmpty = function() {
  this.status = TileStatus.EMPTY;
};

Tile.prototype.setX = function() {
  this.status = TileStatus.X;
};

Tile.prototype.setO = function() {
  this.status = TileStatus.O;
};

Tile.prototype.isEmpty = function() {
  return this.status === TileStatus.EMPTY;
};

Tile.prototype.isX = function() {
  return this.status === TileStatus.X;
};

Tile.prototype.isO = function() {
  return this.status === TileStatus.O;
};

Tile.prototype.toString = function() {

  var sResult;
  if (this.isX()) {
    sResult = ' X ';
  } else if (this.isO()) {
    sResult = ' O ';
  } else if (this.isEmpty()) {
    sResult = ' - ';
  } else {
    sResult = 'ERR';
  }
  return sResult;
}

module.exports = Tile;
