/**
 * Get POD (plain old data) values from Sequelize results.
 *
 * @param rows The result object or array from a Sequelize query's `success` or `complete` operation.
 * @param associations The `include` parameter of the Sequelize query.
 */
function getValuesFromRows (rows, associations) {
    // get POD (plain old data) values
    var values;
    if (rows instanceof Array) {
        // call this method on every element of the given array of rows
        values = [];
        for (var i = 0; i < rows.length; ++i) {
            // recurse
            values[i] = this.getValuesFromRows(rows[i], associations);
        }
    }
    else if (rows) {
        // only one row
        values = rows.dataValues;

        // get values from associated rows
        if (values && associations) {
            for (var i = 0; i < associations.length; ++i) {
                var association = associations[i];
                var propName = association.as;

                // recurse
                values[propName] = this.getValuesFromRows(values[propName], association.include);
            };
        }
    }

    return values;
}

module.exports = {
  getValuesFromRows: getValuesFromRows
}
