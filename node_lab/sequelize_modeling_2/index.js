'use strict';

const SequelizeModels = require("sequelize-models");
var prettyjson = require('prettyjson');

const dbutil = require('./util/dbutil');

var seqModels  = new SequelizeModels({
  // Database connection options
  connection : {
    host     : "localhost",
    dialect  : "mysql",
    username : "root",
    schema   : "dbtest",
    password : "mysecretpassword"
  },

  // Models loading options
  models : {
    autoLoad : false,
    path     : "/models"
  },

  // Sequelize options passed directly to Sequelize constructor
  sequelizeOptions : {
    define : {
      freezeTableName : true,
      underscored     : true
    }
  }
});

////////////////////////////////////////////////////////////////////////////////
// Initial Data
////////////////////////////////////////////////////////////////////////////////

var roleAdmin = { id: '1', name: 'Admin' };
var roleConsumer = { id: '2', name: 'Consumer' };
var roleCommerceAdmin = { id: '3', name: 'CommerceAdmin' };
var roleEmployeeAdmin = { id: '4', name: 'EmployeeAdmin' };

var userDperez = {id: '1', name: 'Diego', surname: 'Perez Molinero', loginname: 'dperez', password: 'alfa1beta2', enabled: true, role_id: 1};

//Main variable to get db and models objects
var dbSchema;

////////////////////////////////////////////////////////////////////////////////
// Operations
////////////////////////////////////////////////////////////////////////////////

seqModels.getSchema()
  .then( schema => {
    // schema.models and schema.db available here
    dbSchema = schema;
  })
  .then(() => {
    dbSchema.db.authenticate();
  })
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .then(() => {
    return dbSchema.db.sync({force: true})
  })
  .then(() => {
    return dbSchema.models.Role.create(roleAdmin);
  })
  .then(() => {
    return dbSchema.models.Role.create(roleConsumer);
  })
  .then(() => {
    return dbSchema.models.Role.create(roleCommerceAdmin);
  })
  .then(() => {
    return dbSchema.models.Role.create(roleEmployeeAdmin);
  })
  .then(() => {
    return dbSchema.models.User.create(userDperez);
  })
  .then(() => {
    console.log("Esquema cargado con los modelos:"+JSON.stringify(dbSchema.models));
    return dbSchema.models.Role.findAll({include: [ { model: dbSchema.models.User, as: 'users' } ]})
  })
  .then(roles => {
    console.log("Roles: --->");
    console.log(prettyjson.render(dbutil.getValuesFromRows(roles,  [ { model: dbSchema.models.User, as: 'users' } ])));
    return true;
  })
  .then(() => {
    return dbSchema.models.User.findAll({include: [ { model: dbSchema.models.Role, as: 'role' } ]})
  })
  .then(users => {
    console.log("Users: --->");
    console.log(prettyjson.render(dbutil.getValuesFromRows(users,  [ { model: dbSchema.models.Role, as: 'role' } ])));
  })
  .then(() => {
    dbSchema.db.close();
    console.log("Connection closed!");
  })
  .catch( err => {
    console.error('Error:', err);
  });
