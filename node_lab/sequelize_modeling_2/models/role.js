module.exports = {

  // Following http://docs.sequelizejs.com/en/latest/docs/models-definition/
  tableName : "role",

  attributes : {
    id: {
      type: "integer",
      primaryKey: true
    },
    name: {
      type: "string"
    }
  },

  // Associations -> http://docs.sequelizejs.com/en/latest/docs/scopes/#associations
  associations : [{
    type    : "hasMany",
    target  : "User",
    options : {
      foreignKey : "role_id",
      as: "users"
    }
  }],

  validate : {},
  indexes  : []
};
