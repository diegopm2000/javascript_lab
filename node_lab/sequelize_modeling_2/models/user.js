module.exports = {

  // Following http://docs.sequelizejs.com/en/latest/docs/models-definition/
  tableName : "user",

  attributes : {
    id: {
      type: "integer",
      primaryKey: true
    },
    name: {
      type: "string"
    },
    surname: {
      type: "string"
    },
    loginname: {
      type: "string"
    },
    password: {
      type: "string"
    },
    enabled: {
      type: "tinyint"
    }
  },

  // Associations -> http://docs.sequelizejs.com/en/latest/docs/scopes/#associations
  associations : [{
    type    : "belongsTo",
    target  : "Role",
    options : {
      foreignKey : "role_id",
      as: "role"
    }
  }],

  validate : {},
  indexes  : []
};
