# Laboratorio de modelado con Sequelize (Parte 2)

En esta segunda parte, se ha utilizado la librería sequelize-models que permite cargar de forma más cómoda los modelos de datos,
desde ficheros separados y sus asociaciones.

Repositorio de sequelize-models en npm

https://www.npmjs.com/package/sequelize-models

Repositorio de sequelize-models en GitHub

https://github.com/gbahamondezc/sequelize-models

Además, se han añadido las siguientes mejoras:

- jsonpretty, para visualizar los json sin tener que hacer stringify y en colores, de una forma mucho mejor.
- librería de utilidad dbutil.js que permite obtener los valores de las columnas de las respuestas de las consultas tal cual.
