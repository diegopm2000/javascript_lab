"use strict";

function asyncSqrt(value, callback) {
    console.log('START execution with value =', value);
    setTimeout(function() {
        callback(value, value * value);
    }, 0 | Math.random() * 100);
}

function* gen(callback) {
    var arr = [0,1,2,3,4,5,6,7,8,9];
    arr.forEach(function (i) {
        yield asyncSqrt(i, callback);
    });
}
var iterator = gen(function (value, result) {
    console.log('END execution with value =', value, 'and result =', result);
    iterator.next();
});
iterator.next();
