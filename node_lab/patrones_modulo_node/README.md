# Laboratorio de patrones módulo en Node JS

Node JS ofrece una implementación del soporte de Common JS para módulos.

Se usa __exports__ para exportar el módulo y __require__ para incluir módulos.

### 0. El módulo más simple

```javascript
// hello.js
console.log('Hello World');
```

```javascript
// app.js
require('hello.js');
```

La primera en la frente, no tira, no es capaz de encontrar hello.js ;-)
Hay que indicar de alguna forma la ruta (de forma relativa o absoluta, no vale con poner tal cual el fichero del que importamos)

### 1. Definimos una (variable) global


```javascript
// foo.js
foo = function () {
  console.log('foo!');
}
```


```javascript
// app.js
require('./foo.js');
foo();
```

No recomendado. No es bueno __contaminar__ el ámbito global de la aplicación.

### 2. Exportar una función anómina

```javascript
// bar.js
module.exports = function () {
  console.log('bar!');
}
```

```javascript
// app.js
var bar = require('./bar.js');
bar();
```

### 3. Exportar una función con nombre

```javascript
// fiz.js
exports.fiz = function () {
  console.log('fiz!');
}
```

```javascript
// app.js
var fiz = require('./fiz.js').fiz;
fiz();
```

### 4. Exportar un objeto anónimo

```javascript
// buz.js
var Buz = function () {};

Buz.prototype.log = function () {
  console.log('buz!');
};

module.exports = new Buz();
```

```javascript
// app.js
var buz = require('./buz.js');
buz.log();
```

### 5. Exportar un objeto con nombre

```javascript
// baz.js
var Baz = function () {};

Baz.prototype.log = function () {
  console.log('baz!');
};

exports.Baz = new Baz();
```

```javascript
// app.js
var baz = require('./baz.js').Baz;
baz.log();
```

### 6. Exportar un prototipo anónimo

```javascript
// doo.js
var Doo = function () {};

Doo.prototype.log = function () {
    console.log('doo!');
}

module.exports = Doo;
```

```javascript
// app.js
var Doo = require('./doo.js');
var doo = new Doo();
doo.log();
```

### 7. Exportar un prototipo con nombre

```javascript
// qux.js
var Qux = function () {};

Qux.prototype.log = function () {
  console.log('baz!');
};

exports.Qux = Qux;
```

```javascript
// app.js
var Qux = require('./qux.js').Qux;
var qux = new Qux();
qux.log();
```

### Conclusiones finales

- __Named exports__ -> Un módulo y podemos exportar más de una cosa (__exports__)
- __Anonymous exports__ -> Interfaz más simple en el cliente (__module.exports__)
