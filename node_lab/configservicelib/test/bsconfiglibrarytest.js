//bseasyconfigtest.js

var expect          = require('chai').expect;
var assert          = require('chai').assert;
var sinon           = require('sinon');
var BSConfigLibrary    = require('../index');
var request         = require("request");

describe("BSConfigLibrary - Test", function() {

  var host;
  var myBSConfigLibrary;

  beforeEach(function() {

    myBSConfigLibrary = new BSConfigLibrary(host);
  });


  describe("Testing the getters and setters properties of main class", function() {

    before(function () {

      host = 'http://localhost:8085';
    });

    it("Testing the getHost", function() {

      expect(myBSConfigLibrary.getHost()).to.equal(host);
    });

    it("Testing the setHost", function() {

      myBSConfigLibrary.setHost(host);
      expect(myBSConfigLibrary.getHost()).to.equal(host);

    });
  });


  describe("Testing the operation getConfig with success", function() {

    var getConfigStub;

    var expectedResponseBody;
    var expectedResponse;
    var expectedResponseError;

    var requestPath;
    var expectedCallParams;

    before(function (done) {

      host = 'http://localhost:8085';
      requestPath = "/configuration/dev/solidity";
      expectedCallParams = {"url": host + requestPath, "json": true};

      expectedResponseBody = {"eris":{"chain":{"name":"serenity"},"node0":{"url":"http://192.168.99.100:1337/rpc","wsrul":"ws://192.168.99.100:1337/socketrpc","dbUrl":"http://192.168.99.100:46657/"},"compilers":{"url":"http://192.168.99.100:9091/compile"},"node1":{},"accountData":{"address":"8365C0F83C7380A3F1C1D92EC2DBBD5FBDC58E50","pubKey":"80045BE51EE9643865BEFA050629A98A412FAA19D14C122F7AC226E34204A4C5","privKey":"3556C655BACF53C278DE09EB80DABF87C7F220B81BF2878903275D00C49FBDE280045BE51EE9643865BEFA050629A98A412FAA19D14C122F7AC226E34204A4C5"},"timeOut":5000,"options":{"callType":2,"fee":0,"gas":1000,"amount":1,"language":"sol"},"nameContract":"smartPayment","solidityFile":"smartPayment","smartPayment":"solidity/contract/","logLevel":"debug","test":true,"maxAttemps":8000,"delayBetweenAttemps":6000,"deployIntervalDelay":6000},"eth":{"node0":{"url":"http://192.168.99.100:8545"},"compilers":{"url":"http://192.168.99.100:9091/compile"},"node1":{},"accountData":{"address":"0x3310317136563b76e72170a34ef4c7c502276eed","password":"password"},"timeOut":5000,"options":{"callType":2,"fee":0,"gas":4600000,"amount":1,"language":"sol"},"nameContract":"smartPayment","solidityFile":"smartPayment","smartPayment":"solidity/contract/","logLevel":"debug","test":true,"maxAttemps":80000,"delayBetweenAttemps":6000,"deployIntervalDelay":30000},"platforms":["eris","eth"],"mongo":{"url":"mongodb://192.168.99.100:27017/contracts","collection":"payments","intervalDelay":6000}};
      expectedResponse = {"statusCode":200, "body": expectedResponseBody};
      expectedResponseError = null;

      getConfigStub = sinon.stub(request, 'get').yields(expectedResponseError, expectedResponse, expectedResponseBody);
      done();
    });

    after(function (done) {
      getConfigStub.restore();
      done();
    });

    it ('Called with adequate parameters', function (done) {

      myBSConfigLibrary.getConfig(requestPath)
      .then(responseBody => {
        try {
          sinon.assert.calledWith(getConfigStub, expectedCallParams);
          done();
        }
        catch (err) {
          done(err);
        }
      })
    })

    it('Get example configuration', function (done) {
        myBSConfigLibrary.getConfig(requestPath)
        .then(responseBody => {
          try {
            expect(JSON.stringify(responseBody)).to.equal(JSON.stringify(expectedResponseBody));
            done();
          }
          catch (err) {
            done(err);
          }
        })
    })

  });

  describe("Testing the operation getConfig with status 404", function() {

    var getConfigStub;

    var expectedResponseBody;
    var expectedResponse;
    var expectedResponseError;

    var requestPath;

    before(function (done) {

      host = 'http://localhost:8085';
      requestPath = "/configuration/dev/solidities";

      expectedResponseBody = {"message": "No configuration defined for solidities"};
      expectedResponse = {"statusCode":404, "message": "No configuration defined for solidities"};
      expectedResponseError = null;

      getConfigStub = sinon.stub(request, 'get').yields(expectedResponseError, expectedResponse, expectedResponseBody);
      done();
    });

    after(function (done) {
      getConfigStub.restore();
      done();
    });

    it ('Getting the 404 error', function (done) {

      myBSConfigLibrary.getConfig(requestPath)
      .then(responseBody => {
        try {
          expect(JSON.stringify(responseBody)).to.equal(JSON.stringify(expectedResponseBody));
          done();
        }
        catch (err) {
          done(err);
        }
      })
    })

  });

  describe("Testing the operation getConfig with general error", function() {

    var getConfigStub;

    var expectedResponseBody;
    var expectedResponse;
    var expectedResponseError;

    var requestPath;

    before(function (done) {

      host = 'http://localhost:8085';
      requestPath = "/configuration/dev/solidities";

      expectedResponseBody = {};
      expectedResponse = {};
      expectedResponseError = "error general!";

      getConfigStub = sinon.stub(request, 'get').yields(expectedResponseError, expectedResponse, expectedResponseBody);
      done();
    });

    after(function (done) {
      getConfigStub.restore();
      done();
    });

    it ('Getting the general error', function (done) {

      myBSConfigLibrary.getConfig(requestPath)
      .then(responseBody => {

      })
      .catch(err => {
        sinon.assert.calledOnce(getConfigStub);
        expect(err).to.equal(expectedResponseError);
        done();
      })

    })

  });

});
