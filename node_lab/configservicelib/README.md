# BSEasyConfig Library

Custom Javascript Library to obtain configurations based on Blockchain Serenity Server Config

The Library was implemented using the next TDD libraries:

- mocha
- chai
- istambul
- eslint

### Installing the Library

```shell
$ npm install
```

### Launching tests

From main folder execute:

```shell
$ npm test
```

### Sonarqube Config

There has been added sonarqube support in the sonar-project.properties

### Example

```javascript
//index.js

var BSConfigLibrary = require("./app/bsconfiglibrary");

var myBSConfigLibrary = new BSConfigLibrary("http://localhost:8085");

myBSConfigLibrary.getConfig('/configuration/dev/solidity');
```
