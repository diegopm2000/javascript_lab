# ColorConverter using unit testing

### Ejecutar los test con:

```shell
$ npm test
```

### Para poder ejecutar los test contra el servidor

Fallan los test contra el servidor si no está levantado, así que hay que levantar el servidor ejecutando:

```shell
$ node app/server.js
```

y luego desde otro terminal, volvemos a pasar los test:

```shell
$ npm test
```
