# Laboratorios de Node JS

### Repositorio principal de laboratorios de Node JS de propósito General.

#### Instalación de Node JS v6

Ejecutamos lo siguiente:

```shell
$ curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
```
Y a continuación:

```shell
$ sudo apt-get install -y nodejs
```

Comprobamos que se haya instalado la versión correctamente:

```shell
$ node -v
v6.9.4
```

Node JS viene con el gestor de paquetes npm ya instalado

Podemos ver la versión instalada:

```shell
$ npm -v
3.10.10
```
