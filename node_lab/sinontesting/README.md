# Laboratorio de Sinon

### Características de Sinon

Sinon es un paquete de javascript para testing que soporta lo siguiente:

- Spies: Ofrecen información sobre las llamadas a las funciones, pero sin modificar su comportamiento.
- Stubs: Similares a los __spies__, pero modifican completamente el comportamiento de las funciones. Con esto podemos modificar completamente el comportamiento de la función.
- Mocks: Permite reemplazar de forma sencilla objetos enteros combinando __spies__ y __stubs__.

Además, ofrece:

- fake timer: Permite simular vencimiento de temporizadores ante funciones que tarden en ejecutarse un tiempo.
- fake XMLHttpRequest and server: Permite simular llamadas a servicios remotos.

### Instalación

Para instalarlo, al tratarse de un paquete npm, se puede hacer así:

```shell
$ npm install sinon
```

### Spies

Ejemplo:

Vamos a obtener los parámetros de llamada a una función llamada "spy"

spy.js

```javascript
var sinon  = require('sinon');

var spy = sinon.spy();

//We can call a spy like a function
spy('Hello', 'World');

//Now we can get information about the call
console.log(spy.firstCall.args); //output: ['Hello', 'World']
```

Con un espía se puede "envolver" una función y gracias a la funcionalidad añadida, poder averiguar cuantas veces se llama.

spy2.js

```javascript
var sinon  = require('sinon');

var user = {
  setName: function(name){
    this.name = name;
  }
}

//Create a spy for the setName function
var setNameSpy = sinon.spy(user, 'setName');

//Now, any time we call the function, the spy logs information about it
user.setName('Darth Vader');

//Which we can see by looking at the spy object
console.log(setNameSpy.callCount); //output: 1

//Important final step - remove the spy
setNameSpy.restore();

```

En este ejemplo, hemos creado un espía para la función __setName__ del objeto __user__, y gracias a la funcionalidad añadida __callCount__ podemos saber las veces que se ha llamado a la función __setName__.

Es importante no olvidarse de quitar el espía una vez hayamos terminado.

En la pŕactica no usaremos mucho los espías salvo por ejemplo dentro de un stub verificar que se ha llamado a una llamada al backend, por ejemplo.

```javascript
function myFunction(condition, callback){
  if(condition){
    callback();
  }
}

describe('myFunction', function() {
  it('should call the callback function', function() {
    var callback = sinon.spy();

    myFunction(true, callback);

    assert(callback.calledOnce);
  });
});
```

### Aserciones en Sinon

Para verificar los resultados en las pruebas unitarias, podemos usar las aserciones de __chai__

```javascript
assert(callback.calledOnce);
```

El problema que tenemos es que por traza solo nos aparecerá true or false, para ello, mejor hacer esto:

```javascript
assert(callback.calledOnce, 'Callback was not called once');
```

También podemos usar las aserciones de sinon:

```javascript
describe('myFunction', function() {
  it('should call the callback function', function() {
    var callback = sinon.spy();

    myFunction(true, callback);

    sinon.assert.calledOnce(callback);
  });
});
```
El uso de las aserciones de sinon nos permite controlar incluso los parámetros de entrada a la función.

Por ejemplo:

- __sinon.assert.calledWith__: Puede ser usado para verificar si la función fue llamada con unos parámetros determinados.

- __sinon.assert.callOrder__: Puede usarse para comprobar el orden de llamada en varias funciones.

También existe un plugin de chai para sinon, que añade las asserts propias de sinon a chai:

https://github.com/domenic/sinon-chai

### Stubs

Aparte de ofrecer la funcionalidad añadida que ofrecen los espías modifican por completo la funcionalidad interna de la función.

```javascript
var stub = sinon.stub();

stub('hello');

console.log(stub.firstCall.args); //output: ['hello']
```
Vamos a ver un ejemplo que como hacer un stub de una función:

La función que tenemos es la siguiente:

```javascript  
function saveUser(user, callback) {
  $.post('/users', {
    first: user.firstname,
    last: user.lastname
  }, callback);
}
```

Normalmente, testear una función que llame mediante Ajax a un servicio remoto es complicado, pero usando un stub, se facilita bastante el asunto:

```javascript
describe('saveUser', function() {
  it('should call callback after saving', function() {

    //We'll stub $.post so a request is not sent
    var post = sinon.stub($, 'post');
    post.yields();

    //We can use a spy as the callback so it's easy to verify
    var callback = sinon.spy();

    saveUser({ firstname: 'Han', lastname: 'Solo' }, callback);

    post.restore();
    sinon.assert.calledOnce(callback);
  });
});
```

Hemos reemplazado la función post de ajax ($) por un stub. De esta forma nunca se llama realmente al servicio remoto, sino que se llama al stub.

Cómo queremos que el callback sea llamado una vez ejecutado el post, lo pasamos como __yields__. Esto significa que el stub llama automáticamente a la primera función pasada como parámetro. Esto imita el comportamiento de $ .post, que llamaría a una devolución de llamada una vez que la solicitud haya terminado.

Adicionalmente, introducimos un spy en el callback para verificar que se llama una vez ejecutado.

En la mayoría de los casos en los que necesitemos un stub, se puede seguir el mismo patrón:

- Tenemos una función problemática, por ejemplo el $.post
- Miramos como funciona para poder imitarla en el test.
- Creamos un stub
- Modificamos el stub para que tenga el comportamiento que queramos en el test.

Un stub no necesita imitar todos los comportamientos, sólo el que vaya a ser objeto del test.

Otro uso común de los stubs es verificar que la función se llama con una serie de parámetros.

Por ejemplo, en este caso vamos a verificar que la función de ajax recibe los parámetros de forma correcta.

```javascript
describe('saveUser', function() {
  it('should send correct parameters to the expected URL', function() {

    //We'll stub $.post same as before
    var post = sinon.stub($, 'post');

    //We'll set up some variables to contain the expected results
    var expectedUrl = '/users';
    var expectedParams = {
      first: 'Expected first name',
      last: 'Expected last name'
    };

    //We can also set up the user we'll save based on the expected data
    var user = {
      firstname: expectedParams.first,
      lastname: expectedParams.last
    }

    saveUser(user, function(){} );
    post.restore();

    sinon.assert.calledWith(post, expectedUrl, expectedParams);
  });
});
```

Hemos usado __calledWith__ para comprobar que la función se ha llamado con los parámetros correctos.

Hay otra alternativa para probar esto, y es usar __fake XMLHttpRequest__:

https://codeutopia.net/blog/2015/03/21/unit-testing-ajax-requests-with-mocha/

### Mocks

Los mocks son una aproximación diferente a los stubs. Su uso principal es para stubear más de una función dentro de un objeto. Si śolo se necesita stubear una función, la aproximación de un stub es más sencilla de implementar.

```javascript
describe('incrementStoredData', function() {
  it('should increment stored value by one', function() {
    var storeMock = sinon.mock(store);
    storeMock.expects('get').withArgs('data').returns(0);
    storeMock.expects('set').once().withArgs('data', 1);

    incrementStoredData();

    storeMock.restore();
    storeMock.verify();
  });
});
```

Con el uso de mock, definimos una serie de llamadas dentro de la función y sus resultados esperados, mediante una api fluída. Al final se llama a __verify__ donde se comprueba los resultados.

### Buenas prácticas con Sinon

Si reemplazamos una función con un test-double, usaremos __sinon.test()__.

En anteriores ejemplos hemos usado stub.restore() o mock.restore(), pero aparte de que se nos puede olvidar ponerlo, puede haber algún fallo que evite que se llame a estas funciones.

Este problema, se evita de esta manera:

```javascript
it('should do something with stubs', sinon.test(function() {
  var stub = this.stub($, 'post');

  doSomething();

  sinon.assert.calledOnce(stub);
});
```

Otra parte importante de usar esto, es que podemos usar this.stub, this.spy o this.mock, usando el sandboxing de sinon. De esta forma, tenemos el autolimpiado de forma directa, evitando que si un test dió fallo, el resto de test que se ejecuten a continuación, fallen también por no haberse limpiado correctamente.

### Sinon no es mágico

Un __espía__, básicamente lo que hace es envolver una función:

```javascript
//A simple spy helper
function createSpy(targetFunc) {
  var spy = function() {
    spy.args = arguments;
    spy.returnValue = targetFunc.apply(this, arguments);
    return spy.returnValue;
  };

  return spy;
}

//Let's spy on a simple function:
function sum(a, b) { return a + b; }

var spiedSum = createSpy(sum);

spiedSum(10, 5);

console.log(spiedSum.args); //Output: [10, 5]
console.log(spiedSum.returnValue); //Output: 15
```

Con esto hemos obtenido una implementación sencilla de un espía, con muchas carencias como soporte de asserts entre otros, pero vale como ejemplo.

Un __stub__, lo que hace es reemplazar una función por otra.

```javascript
var stub = function() { };

var original = thing.otherFunction;
thing.otherFunction = stub;

//Now any calls to thing.otherFunction will call our stub instead
```

Con las consiguientes carencias, como falta de todo lo que ofrecen los espías, no podemos volver a la función original con el restore ni tenemos el soporte de asserts.

Un __mock__ simplemente combina la funcionalidad de los espías y los stubs, permitiendo usar sus características de diferente forma.

En definitiva, que es perfectamente posible implementar de forma sencilla la funcionalidad para la mayor parte de lo que se necesita, pero por otra parte, ya nos la ofrece probada y lista para se usada.
