'use strict'

const uuidV1 = require('uuid/v1');
const crypto = require('crypto');
const fs = require('fs');
var request = require('request');

console.log("Ripple FX Connector Client");

//Función de lectura de fichero de forma síncrona
function readFile(path) {
  try {
    return fs.readFileSync(path)
  } catch (e) {
    throw new Error(`Unable to read ${path}`)
  }
}

//Función de firmado
function sign(alg = 'RSA-SHA256', privateKey, nonce, uri, method, payload) {

  const signer = crypto.createSign(alg);
  void signer.write(nonce);
  void signer.write(uri);
  void signer.write(method)
  void signer.write(JSON.stringify(payload || {}))
  void signer.end();
  return signer.sign(privateKey, 'base64')
}

//Funcion que para un certificado elimina la cabecera, el pie y los saltos de línea
function serializeCert(pemCert) {
  return pemCert.replace(/\r\n/g, '\n')
    .replace(/-----BEGIN.*\n/, '')
    .replace(/-----END.*\n?/, '')
    .replace(/\n/g, '');
}

//1. Generamos un v1 UUID (time-based)
var myNonce = uuidV1();
console.log("myNoonce:"+myNonce);

//2. Probamos a leer los tres ficheros de certificados PEM
var providerAdminCrt = readFile('./certificates/provider-admin-crt.pem');
//console.log('fichero leido providerAdminCrt:'+providerAdminCrt);
var providerAdminKey = readFile('./certificates/provider-admin-key.pem');
//console.log('fichero leido providerAdminKey:'+providerAdminKey);
var providerCaCert = readFile('./certificates/provider-ca-crt.pem');
//console.log('fichero leido providerCaCert:'+providerCaCert);

//3. Pasamos los certificados a buffer
var providerAdminCrtBuffered =  {cert: providerAdminCrt};
console.log('providerAdminCrtBuffered'+providerAdminCrtBuffered);
var providerAdminKeyBuffered = {key: providerAdminKey};
console.log('providerAdminKeyBuffered'+providerAdminKeyBuffered);
var providerCaCertBuffered = {ca: providerCaCert};
console.log('providerCaCertBuffered'+providerCaCertBuffered);

//4. Obtenemos la signatura
var mySign = sign('RSA-SHA256', providerAdminKeyBuffered, myNonce, '/rates', 'GET', "");
console.log('mySign:'+mySign);

//5. Hay que serializar el certificado, quitando los saltos de línea
console.log('providerAdminCrt:'+providerAdminCrt);
var providerAdminCrtSerialized = serializeCert(''+providerAdminCrt);
console.log('providerAdminCrtSerialized:<<'+providerAdminCrtSerialized+">>");


var providerAdminCrtExtractedFromGoodQuery = "MIIC6jCCAdKgAwIBAgIJAJGjkUUtXUc3MA0GCSqGSIb3DQEBBQUAMBoxGDAWBgNVBAMTD2V1ci1iYW5rLmNvbS1jYTAeFw0xNzA1MjkxNDU1MDFaFw0yNzA1MjcxNDU1MDFaMBcxFTATBgNVBAMTDGV1ci1iYW5rLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOWWt31krSRNYGAt4/Ed5Vw8CNHz9mN+en0rlvDRw1xqVITz8LqyM0ydGOPwNjYbdk94DrOvawZX3eUKFkciCX7xgp/Xti0PHDA2JyRDGHeAdvCTMUj5d4lhxmt3COs8ogOkxe8xt5owZVuHvuvbE9htux6xy8TBphMPp2hcAPFnmucOf4BrWPhTZTNW1P8tGfAwTSgVR8vBcAelWyHVjG29SllbYMCuIzsejG556czUzfOQP2sIQzYFgIkKQD0V277MPWctr8zgaIQBtk8M4mvfSZlM2ef/o+MzD6S/rWX0j+TWW8S3jB5r+G3Mhjld05IXrrCJc4ijnhV04DnvPtECAwEAAaM2MDQwCQYDVR0TBAIwADAnBgNVHREEIDAeggxldXItYmFuay5jb22CDiouZXVyLWJhbmsuY29tMA0GCSqGSIb3DQEBBQUAA4IBAQBt5+AxWLq37uEbCQGLYNjCb7FcSmxx0g8bMrN2xPZTc+GsSpykzUL1WsYQ/prKMV/KdOukT+Nt/RUET1fpQwhlkpTk23tj/HZmKbHcOeuPW5oLt3v9+oR6Ol4fVCp7lcE/ZqSSxsqCcVD8YcqogLLng6u/GgiyJ3MOc6DXSURPxJfSQ5BSWX2jljkG+pT71tfAScwkgKDXiYRzHd3Gi0tDNTnqrB4hhlAmqlbpiIcgr4BzwRsBmMVDSMXY62ukupu+uiJTvYECEnTWGhFnTgdsOJzYotsg8rMVH1curjSLDPU4ubE8KHPF2GO2Fb2cCQvc8QdkzE9LkyL0Lv8cMo5x";

var bIguales = providerAdminCrtSerialized == providerAdminCrtExtractedFromGoodQuery;
console.log("son iguales los ripple_cert:"+bIguales);


var options = {
  uri: 'https://eur-bank.com:3999/rates',
  method: 'GET',
  ca: providerCaCertBuffered.ca,
  cert: providerAdminCrtBuffered.cert,
  key: providerAdminKeyBuffered.key,
  headers:  {
    ripple_alg: 'RSA-SHA256',
    ripple_nonce: myNonce,
    ripple_cert: providerAdminCrtSerialized,
    ripple_signature: mySign
  }
}

console.log("options:"+JSON.stringify(options));

//5. Hacemos la request

request(options,
function (error, response, body) {
    if (error) {
      return console.error('request failed:', error);
    }
    console.log('Request successful!  Server responded with:', body);
})

console.log("Fin de programa");
