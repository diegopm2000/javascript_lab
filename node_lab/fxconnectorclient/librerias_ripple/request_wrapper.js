// ©Copyright 2016-2017 Ripple Labs Inc., All Rights Reserved.
// Ripple Labs, Inc. Proprietary and Confidential
// Unauthorized copying and distribution strictly prohibited
'use strict'

const _ = require('lodash')
const fs = require('fs')
const request = require('request')
const {signRequest} = require('auth')
const Getopt = require('node-getopt')

require('request-to-curl');

const getopt = new Getopt([
  ['L', 'cert=ARG', 'Cert'],
  ['L', 'key=ARG', 'Key'],
  ['m', 'ca=ARG+', 'Trusted CA'],
  ['L', 'method=ARG', 'Http method'],
  ['L', 'body=ARG', 'JSON body'],
  ['h', 'help', 'Help']
])

function readFile(path) {
  try {
    return fs.readFileSync(path)
  } catch (e) {
    throw new Error(`Unable to read ${path}`)
  }
}

function jsonParse(json) {
  try {
    return JSON.parse(json)
  } catch (e) {
    throw new Error('Body not valid JSON.')
  }
}

function formatBody(body) {
  if (_.isUndefined(body)) {
    return ''
  }

  if (_.isObject(body)) {
    return JSON.stringify(body, null, 2)
  }

  return body
}

function main(opt) {
  const argOptions = opt.options
  const uri = _.first(opt.argv)
  const cert = argOptions.cert
  const key = argOptions.key
  const ca = argOptions.ca
  const method = argOptions.method || 'GET'
  const reqBody = argOptions.body

  if (!uri) {
    throw new Error('URI required.')
  }

  if (!cert) {
    throw new Error('Certificate required.')
  }

  if (!key) {
    throw new Error('Key required.')
  }

  if (!ca) {
    throw new Error('CA array required.')
  }

  if (method === 'GET' && reqBody) {
    throw new Error('Cannot have body with GET request')
  }

  const authOptions = {
    cert: readFile(cert),
    key: readFile(key)
  }

  const requestOptions = {
    uri,
    method,
    ca: ca.map(readFile)
  }

  if (reqBody) {
    _.set(requestOptions, 'json', jsonParse(reqBody))
  }

  const options = _.assign(requestOptions, authOptions)

console.log("Diego -> requestOptions:"+JSON.stringify(requestOptions));
console.log("Diego -> authOptions:"+JSON.stringify(authOptions));

  const signedRequest = _.assign(options,
    signRequest(requestOptions, authOptions))

console.log("Diego -> signedRequest:"+JSON.stringify(signedRequest));

  request(signedRequest, (err, res) => {
    if (err) {
      console.error('Error making request\n', err)
      process.exit(1)
    } else {

	//console.log(res.request.req.toCurl());

      const body = res.body ? `\n${formatBody(res.body)}` : ''
      const status = res.statusCode ? `\n${res.statusCode}` : ''

      if (res.statusCode > 299) {
        console.error('Error making request', status, body)
        process.exit(1)
      } else {
        console.log('Success', status, body)
      }
    }
  })
}

const opt = getopt.parse(process.argv.slice(2))
main(opt)
