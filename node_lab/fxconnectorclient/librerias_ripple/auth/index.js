// ©Copyright 2016-2017 Ripple Labs Inc., All Rights Reserved.
// Ripple Labs, Inc. Proprietary and Confidential
// Unauthorized copying and distribution strictly prohibited
'use strict'
const crypto = require('crypto')
const _ = require('lodash')
const ca_verifier = require('x509')
const assert = require('assert')
const uuid = require('uuid')
const url = require('url')

function forbiddenResponse(res) {
  res.status(401).send('Authentication failed')
}
function isValidUUIDv1(id) {
  const stringId = String(id)
  const hex = '[0-9a-f]'
  const validationPatternTime = `${hex}{8}-${hex}{4}-1${hex}{3}`
  const validationPatternMac = `${hex}{4}-${hex}{12}`
  const validationPattern = `${validationPatternTime}-${validationPatternMac}`
  const validationRegExp = new RegExp(validationPattern, 'i')
  return Boolean(stringId.match(validationRegExp))
}

function getTimestampFromUUID(id) {
  const uuidParts = id.split('-')

  // assemble uuid parts and remove the version nibble
  const uuidHexTimestampNanos = (
    uuidParts[2] +
    uuidParts[1] +
    uuidParts[0]
  ).substr(1)

  // convert hex string representation to integer, and discard nanoseconds
  const uuidTimestampMillis = parseInt(uuidHexTimestampNanos, 16) / 10000

  // Adjust by the UUID V1 Gregorian Time Offset
  // (from the rfc the uuid v1 start date is:
  // '00:00:00.00, 15 October 1582')
  const gregorianUUIDOffset = 12219292800000
  const utcTimestampMillis = uuidTimestampMillis - gregorianUUIDOffset
  return utcTimestampMillis
}

function serializeCert(pemCert) {
  return pemCert.replace(/\r\n/g, '\n')
    .replace(/-----BEGIN.*\n/, '')
    .replace(/-----END.*\n?/, '')
    .replace(/\n/g, '')
}

function deserializeCert(base64Cert) {
  const header = '-----BEGIN CERTIFICATE-----\n'
  const footer = '\n-----END CERTIFICATE-----\n'
  const certWithNewlines = base64Cert.replace(/(.{64})/g, '$1\n')
  return header + certWithNewlines.trim() + footer
}

function checkCert(cert, ca) {
  return new Promise((resolve, reject) => {
    ca_verifier.verify(cert, ca, function(err) {
      if (err !== null) {
        return reject(err)
      }
      return resolve(true)
    })
  })
  .catch(() => false)
}

function isCertificateTrusted(cert, caList) {
  if (!caList || caList.length === 0) {
    return false
  }
  return checkCert(cert, caList[0])
  .then(valid => valid ? true : isCertificateTrusted(cert, _.tail(caList)))
}

function sign(alg = 'RSA-SHA256', privateKey, nonce, uri, method, payload) {
  const signer = crypto.createSign(alg)
  signer.write(nonce)
  signer.write(uri)
  signer.write(method)
  signer.write(JSON.stringify(payload || {}))
  signer.end()
  return signer.sign(privateKey, 'base64')
}

function verify(alg = 'RSA-SHA256', publicKey, signature,
  nonce, uri, method, payload) {
  const verifier = crypto.createVerify(alg)
  verifier.write(nonce)
  verifier.write(uri)
  verifier.write(method)
  verifier.write(JSON.stringify(payload || {}))
  verifier.end()
  return verifier.verify(publicKey, signature, 'base64')
}

function verifyRequest(config, log) {
  return (req, res, next) => {
    if (req.originalUrl === '/health') {
      return next()
    }
    const algorithm = req.headers.ripple_alg
    if (!_.includes(crypto.getHashes(), algorithm)) {
      log.debug(`Algorithm ${algorithm} is not supported`)
      return forbiddenResponse(res)
    }

    const rippleNonce = req.headers.ripple_nonce
    if (isValidUUIDv1(rippleNonce)) {
      const nonceTimestamp = getTimestampFromUUID(rippleNonce)
      const currentTimestamp = Date.now()
      const nonceDelta = Math.abs(currentTimestamp - nonceTimestamp)
      const nonceTimeout =
        (config.timestampWindow || req.socket._idleTimeout || 120000)
      if (nonceDelta > nonceTimeout) {
        log.debug(`Invalid nonce timestamp: ${nonceTimestamp} ` +
                       `is ${nonceDelta} millis from the current time.`)
        return forbiddenResponse(res)
      }
    } else {
      log.debug(`Invalid nonce format ${rippleNonce}, must be uuid V1`)
      return forbiddenResponse(res)
    }

    const cert = Buffer.from(deserializeCert(req.headers.ripple_cert))
    let fingerprint
    try {
      const parsedCert = ca_verifier.parseCert(String(cert))
      fingerprint = parsedCert.fingerPrint
    } catch (err) {
      log.debug(`Invalid certificate: ${err.message}`)
      return forbiddenResponse(res)
    }

    // Verify that we trust the certificate passed in the HTTP header
    return isCertificateTrusted(cert,
      _.isArray(config.server.ca) ? config.server.ca : [config.server.ca])
    .then(trusted => {
      if (trusted) {
        // Verify the signature in the HTTP header
        const valid = verify(algorithm, cert, req.headers.ripple_signature,
          req.headers.ripple_nonce, req.originalUrl, req.method, req.body)
        if (!valid) {
          log.debug(`Invalid signature on ${req.originalUrl}`)
          return forbiddenResponse(res)
        }
        req.getClientFingerprint = function() {
          return fingerprint
        }
        return next()
      } else {
        log.debug('Untrusted certificate')
        return forbiddenResponse(res)
      }
    })
    .catch(() => forbiddenResponse(res))
  }
}

function signRequest(options, authOptions) {
  if (!authOptions.cert || !authOptions.key) {
    throw new Error('signRequest authOptions must include cert and key')
  }

  const auth = _.defaults({}, authOptions, {
    alg: 'RSA-SHA256'
  })

  const certificate = _.isString(auth.cert) ?
    Buffer.from(auth.cert) : auth.cert
  const signatureAlgorithm = auth.alg
  assert(_.includes(crypto.getHashes(), signatureAlgorithm),
    'Unsupported signature algorithm')
  const nonce = uuid.v1()

  const parsedUrl = url.parse(options.uri || options.url)
  const payload = _.isBoolean(options.json) ? options.body : options.json
  const signature = sign(signatureAlgorithm, auth.key, nonce, parsedUrl.path,
    options.method, payload)

  const serializedCert = serializeCert(String(certificate))
  assert(!_.includes(serializedCert, '\r'),
    'Includes unsupported carriage return "\\r"')

  return _.merge({}, options, {
    headers: {
      ripple_alg: signatureAlgorithm,
      ripple_nonce: nonce,
      ripple_cert: serializedCert,
      ripple_signature: signature
    }
  })
}

module.exports = {
  isCertificateTrusted,
  verifyRequest,
  signRequest
}

module.exports._test = {
  isValidUUIDv1,
  getTimestampFromUUID
}
