# WebApp de Ejemplo con Yeoman

Se ha usado yeoman para la generación de la estructura de la aplicación.

Versión de Yeoman: 1.8.5

Generadores usados:

generator-webapp

### 1. Para generar la estructura, hemos instalado previamente en generador

```shell
$ npm install --global yo gulp-cli bower generator-webapp
```

### 2. A continuación generamos la aplicación

```shell
$ yo webapp
```

Hemos elegido Bootstrap y TDD

### 3. Arrancar el servidor

```shell
$ gulp serve
```
